////////////////////////////////////////////////////////////////////////////////
/**
 * Copyright (C) 2004-2006 G33X Nexus Entertainment (http://gne.g33xnexus.com/)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////
/**
 * @file	global.h
 * @author	David H. Bronke
 * @email	david.bronke@g33xnexus.com
 *
 * @brief   Master include file for Requiem For Innocence: Precursors.
 *
 * All header files should include this one as the first file included.
 * 
 * Please follow this heirarchy in including header files:
 *	- The first include in EVERY header file is global.h
 *	- The first include in EVERY source file is its associated header file
 */
////////////////////////////////////////////////////////////////////////////////


#ifndef __Precursors__Global_h__
#define __Precursors__Global_h__

/* We expect to use Crystal Space everywhere. */
#include <cssysdef.h>

/* MSVC users do not run configure, so use special MSVC configuration file. */
#if defined(CS_WIN32_CSCONFIG)
#include "config-msvc.h"
#else
#include "config.h"
#endif

/* C++ library includes */
//#include <cstdlib>


//==============================================================================
// CS includes

// Containers
#include "csutil/csstring.h"
#include "csutil/array.h"
#include "csutil/set.h"
#include "csutil/list.h"
#include "csutil/hash.h"
#include "csutil/redblacktree.h"

// References
#include "csutil/ref.h"
#include "csutil/weakref.h"
#include "csutil/refarr.h"


//==============================================================================
// Other project-wide includes
//#include "util/ConstrainedValue.h"

/* Insert additional project-specific declarations here. */


////////////////////////////////////////////////////////////////////////////////
/**
 * @brief The Precursors namespace
 *
 * Everything in the project will be declared inside
 * this namespace.
 */
namespace Precursors
{
	/// The current version of Precursors
	//const csString VERSION = PACKAGE_VERSION + csString (" Build ") + SVN_REV;
	#define VERSION "0.5.0 Build 3"

	/// The authors of Precursors
	//const csString AUTHORS = "G33X Nexus Entertainment";
	#define AUTHORS "G33X Nexus Entertainment"

	/**
	 * @brief The namespace containing our behaviour layer and its behaviours.
	 */
	namespace BehaviourLayer
	{
		/**
		 * @brief The namespace containing the networked level behaviours.
		 */
		namespace NetworkLevel {}

		/**
		 * @brief The namespace containing the controlled entity behaviours.
		 */
		namespace ControlledEntity {}

		/**
		 * @brief The namespace containing the user interface behaviours.
		 */
		namespace UserInterface {}

		/**
		 * @brief The namespace containing the system behaviours.
		 */
		namespace Systems {}
	} // end namespace BehaviourLayer
} // end namespace Precursors

/**
 * @brief The namespace for the policies used by the ConstrainedValue class.
 */
namespace Policies {}


// Include the helper functions and macros - the little things that make
// our lives easier.
#include "helpers.h"

#endif // __GLOBAL_H__

