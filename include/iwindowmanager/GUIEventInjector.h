////////////////////////////////////////////////////////////////////////////////
/**
 * Copyright (C) 2004-2006 G33X Nexus Entertainment (http://gne.g33xnexus.com/)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////
/**
 * @file GUIEventInjector.h
 *
 * @author Seth Yastrov (syastrov@gmail.com)
 *
 * @brief Declaration of GUIEventInjector, CEGUI event injector
 */
////////////////////////////////////////////////////////////////////////////////


#ifndef __GUIEventInjector_h__
#define __GUIEventInjector_h__

#include "global.h"

#include "csutil/refcount.h"
#include "csutil/objreg.h"
#include "iutil/event.h"
#include "iutil/eventq.h"

// Precursors Includes
#include "iwindowmanager/PrecursorsEvents.h"

// Forward declarations
struct iObjectRegistry;
struct iEvent;
struct iCEGUI;


namespace Precursors
{
	////////////////////////////////////////////////////////////////////////////
	/**
	 * @brief CEGUI Event Injector.
	 *
	 * This class injects CEGUI events into the CS event system.
	 */
	class GUIEventInjector :
		public scfImplementation1<GUIEventInjector, iEventPlug>
	{
	private:
		////////////////////////////////////////////////////////////////////////
		/// Member variables.
		/// @{

		/// The object registry.
		iObjectRegistry* object_reg;

		/// The CEGUI plugin.
		csWeakRef<iCEGUI> cegui;

		/// The event outlet
		csWeakRef<iEventOutlet> eventOutlet;

		/// The name registry
		csWeakRef<iEventNameRegistry> name_reg;

		/// The event connection
		CEGUI::Event::Connection connection;

		/// @}


		////////////////////////////////////////////////////////////////////////
		/// Event Handlers
		/// @{

		/// Called when the window is clicked.
		bool OnClicked (const CEGUI::EventArgs& args);

		/// Called when a key is pressed in the window.
		bool OnKeyPress (const CEGUI::EventArgs& args);

		/// @}


		/// Send a menu event to the application.
		void SendMenuEvent (const char* action);

	public:
		////////////////////////////////////////////////////////////////////////
		/// Constructors and destructors
		/// @{

		/// Constructor.
		GUIEventInjector (iObjectRegistry* object_reg);

		/// Constructor.
		GUIEventInjector (iObjectRegistry* object_reg, CEGUI::Window* window);

		/// Destructor.
		virtual ~GUIEventInjector ();

		/// @}


		////////////////////////////////////////////////////////////////////////
		/// Initialization
		/// @{

		/**
		 * Set up the events for this event injector.
		 */
		void SetupEvents ();

		/**
		 * Set up the events for this event injector.
		 *
		 * @param window The CEGUI window with which associate this event injector.
		 */
		void SetupEvents (CEGUI::Window* window);

		/// @}


		////////////////////////////////////////////////////////////////////////
		/// iEventPlug implementation
		/// @{

		virtual void EnableEvents (unsigned, bool)
		{}

		virtual unsigned GetPotentiallyConflictingEvents ()
		{ return 0; }

		virtual unsigned QueryEventPriority (unsigned /*iType*/)
		{ return 150; }

		/// @}
	}; // end class Precursors
} // end namespace Precursors

#endif // __GUIEventInjector_h__
