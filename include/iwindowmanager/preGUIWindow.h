////////////////////////////////////////////////////////////////////////////////
/**
 * Copyright (C) 2004-2006 G33X Nexus Entertainment (http://gne.g33xnexus.com/)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////
/**
 * @file preGUIWindow.h
 * @author David Bronke (whitelynx@gmail.com)
 *
 * @brief Declaration of the Precursors GUI window base class.
 */
////////////////////////////////////////////////////////////////////////////////


#ifndef __preGUIWindow_h__
#define __preGUIWindow_h__

// Precursor Include
#include "iwindowmanager/iPreGUIWindow.h"
#include "iwindowmanager/iPreWindowManager.h"

// CrystalSpace Includes
#include "csutil/weakref.h"
#include "csutil/scf_implementation.h"



// Forward declarations
struct iObjectRegistry;
struct iCEGUI;


/**
 * The content loader interface.
 */
struct preGUIWindow : public scfImplementation1<preGUIWindow, iPreGUIWindow>
{
protected:
	////////////////////////////////////////////////////////////////////////
	/// Member variables.
	/// @{

	/// The object registry.
	iObjectRegistry* object_reg;

	/// The Precursors window manager.
	csWeakRef<iPreWindowManager> windowManager;

	/// A pointer to the CEGUI interface.
	csWeakRef<iCEGUI> cegui;

	/// A pointer to the CEGUI window manager.
	CEGUI::WindowManager* ceguiWinMgr;

	/// A pointer to the CEGUI window object.
	CEGUI::Window* window;

	/// The event connection for keyboard events.
	CEGUI::Event::Connection keyboardCon;

	/// The event connection for mouse events.
	CEGUI::Event::Connection mouseCon;

	/// @}

public:
	////////////////////////////////////////////////////////////////////////
	/// Constructors and destructors
	/// @{

	/// Constructor.
	preGUIWindow (iObjectRegistry* object_reg);

	/// Constructor.
	preGUIWindow (iObjectRegistry* object_reg, const char* windowPath);

	/// Destructor.
	virtual ~preGUIWindow ();

	/// @}


	////////////////////////////////////////////////////////////////////////
	/// Window Management Methods
	/// @{

	/**
	 * Hide this window.
	 */
	virtual void Hide ();

	/**
	 * Display this window.
	 */
	virtual void Show ();

	/**
	 * Toggle the visibility of this window.
	 * @return true if the window is now visible, false otherwise.
	 */
	virtual bool Toggle ();

	/**
	 * Get a pointer to the underlying CEGUI window.
	 */
	virtual CEGUI::Window* GetWindow ();

	/**
	 * Send this window a message.
	 * @return true on success, false otherwise.
	 */
	virtual bool SendMessage (const char* message, csHash<csVariant, csString> parameters);

	/// @}


	////////////////////////////////////////////////////////////////////////
	/// Event Handlers
	/// @{

	/// Called when the window is clicked.
	virtual bool OnClicked (const CEGUI::EventArgs& args);

	/// Called when a key is pressed in the window.
	virtual bool OnKeyPress (const CEGUI::EventArgs& args);

	/// Perform the action associated with the given control.
	virtual bool Action (const CEGUI::String windowName);

	/// @}
}; // end struct preGUIWindow

#endif // __preGUIWindow_h__

