////////////////////////////////////////////////////////////////////////////////
/**
 * Copyright (C) 2004-2006 G33X Nexus Entertainment (http://gne.g33xnexus.com/)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////
/**
 * @file PrecursorsEvents.h
 *
 * @author Seth Yastrov (syastrov@gmail.com)
 *
 * @brief Custom Precursors Events
 */
////////////////////////////////////////////////////////////////////////////////


#ifndef __PrecursorsEvents_h__
#define __PrecursorsEvents_h__


/// The definition of the menu action event.
#define preevMenuAction(reg)         \
	(csEventNameRegistry::GetID((reg), "precursors.menu.action"))

#endif // __PrecursorsEvents_h__
