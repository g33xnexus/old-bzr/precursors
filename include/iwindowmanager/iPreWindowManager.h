////////////////////////////////////////////////////////////////////////////////
/**
 * Copyright (C) 2004-2006 G33X Nexus Entertainment (http://gne.g33xnexus.com/)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////
/**
 * @file iPreWindowManager.h
 * @author David Bronke (whitelynx@gmail.com)
 *
 * @brief Declaration of the Precursors window manager interface.
 */
////////////////////////////////////////////////////////////////////////////////


#ifndef __iPreWindowManager_h__
#define __iPreWindowManager_h__

#include "csutil/scf_interface.h"

struct iPreGUIWindow;

namespace CEGUI
{
	struct Window;
}


/**
 * The content loader interface.
 */
struct iPreWindowManager : public virtual iBase
{
	SCF_INTERFACE(iPreWindowManager, 2, 0, 0);

	/**
	 * Set the given window as the root window.
	 */
	virtual void SetRootWindow (const char* window) = 0;

	/**
	 * Toggle the root between two different windows.
	 * @return true if the root window was set to windowName1; false otherwise.
	 */
	virtual bool ToggleRootWindow (iPreGUIWindow* window1, iPreGUIWindow* window2) = 0;

	/**
	 * Get a pointer to the root window.
	 */
	virtual iPreGUIWindow* GetRootWindow () = 0;

	/**
	 * Set a property on a child the given window.
	 */
	virtual void SetChildProperty (const char* windowName, const char* property, const char* value) = 0;

	/**
	 * Find the given window, if it is loaded, and return a pointer to it.
	 *
	 * @return a pointer to the window, or NULL if the window is not found.
	 */
	virtual iPreGUIWindow* FindWindow (const char* windowName) = 0;

	/**
	 * Load the given window and return a pointer to it.
	 */
	virtual iPreGUIWindow* LoadWindow (const char* windowName) = 0;

	/**
	 * Load the given window layout and return a pointer to the window.
	 */
	virtual CEGUI::Window* LoadCEGUILayout (const char* layoutName) = 0;

	/**
	 * Load the given window and return a pointer to it.
	 */
	virtual CEGUI::Window* LoadCEGUIWindow (const char* windowName) = 0;

	/**
	 * Hide all windows except the root window.
	 */
	virtual void HideAll () = 0;

	/**
	 * Hide the mouse cursor and pass mouse events to the game logic.
	 */
	virtual void EnableMouseLook() = 0;

	/**
	 * Show the mouse cursor and pass mouse events to the GUI.
	 */
	virtual void DisableMouseLook() = 0;

}; // end struct iPreWindowManager

#endif // __iPreWindowManager_h__

