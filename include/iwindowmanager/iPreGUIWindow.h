////////////////////////////////////////////////////////////////////////////////
/**
 * Copyright (C) 2004-2006 G33X Nexus Entertainment (http://gne.g33xnexus.com/)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////
/**
 * @file iPreGUIWindow.h
 * @author David Bronke (whitelynx@gmail.com)
 *
 * @brief Declaration of the Precursors GUI window interface.
 */
////////////////////////////////////////////////////////////////////////////////


#ifndef __iPreGUIWindow_h__
#define __iPreGUIWindow_h__

#include "csutil/scf_interface.h"

// Forward declarations
struct csVariant;

namespace CEGUI
{
	struct Window;
}


/**
 * The content loader interface.
 */
struct iPreGUIWindow : public virtual iBase
{
	SCF_INTERFACE(iPreGUIWindow, 0, 0, 1);

	////////////////////////////////////////////////////////////////////////
	/// Window Management Methods
	/// @{

	/**
	 * Hide this window.
	 */
	virtual void Hide () = 0;

	/**
	 * Display this window.
	 */
	virtual void Show () = 0;

	/**
	 * Toggle the visibility of this window.
	 * @return true if the window is now visible, false otherwise.
	 */
	virtual bool Toggle () = 0;

	/**
	 * Get a pointer to the underlying CEGUI window.
	 */
	virtual CEGUI::Window* GetWindow () = 0;

	/**
	 * Send this window a message.
	 * @return true on success, false otherwise.
	 */
	virtual bool SendMessage (const char* message, csHash<csVariant, csString> parameters) = 0;

	/// @}
}; // end struct iPreGUIWindow

#endif // __iPreGUIWindow_h__

