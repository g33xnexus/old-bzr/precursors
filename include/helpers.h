#ifndef __helpers_h__
#define __helpers_h__

/**
 * A macro to get the desired propertyclass and stick it in the given variable.
 * Asserts that the pointer is valid. Mainly to be used in GetPropertyClasses ().
 * Assumes that the entity is named entity; in cases where it's not, we may
 * write another macro.
 */
#define GET_PROP_CLASS(VAR, CLASS)						\
	if (!VAR.IsValid ())								\
	{													\
		VAR = CEL_QUERY_PROPCLASS_ENT (entity, CLASS);	\
		CS_ASSERT (VAR.IsValid ());						\
	} 

#endif /* __helpers_h__ */
