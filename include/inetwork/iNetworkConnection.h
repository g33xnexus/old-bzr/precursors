////////////////////////////////////////////////////////////////////////////////
/**
 * @file iNetworkConnection.h
 * @author Christopher Case (macguyvok@gmail.com)
 * @author David Bronke (whitelynx@gmail.com)
 *
 * @brief Declaration of the network connection interface, which represents
 * a network connection between the local machine and a remote machine.
 */
////////////////////////////////////////////////////////////////////////////////


#ifndef __iNetworkConnection_h__
#define __iNetworkConnection_h__

#include "csutil/scf_interface.h"

struct iNetworkPacket;
struct iNetworkPacketFactory;


/**
 * @brief The network connection interface.
 *
 * This interface represents a network connection between two hosts.
 */
struct iNetworkConnection : public virtual iBase
{
public:
	SCF_INTERFACE(iNetworkConnection, 1, 0, 0);

	/**
	 * Set the packet factory. This object will be used to create all packet objects.
	 */
	virtual void SetPacketFactory (iNetworkPacketFactory* factory) = 0;
	
	/**
	 * Send a packet.
	 */
	virtual void Send (iNetworkPacket* packet) = 0;
	
	/**
	 * Receive a packet.
	 */
	virtual csPtr<iNetworkPacket> Receive (bool blocking = false) = 0;
	
	/**
	 * Disconnect and destroy connection. Returns True if successful.
	 */
	virtual bool Disconnect () = 0;
	
	/**
	 * Limit the amount of bandwith used by this connection.
	 */
	virtual void Limit (unsigned int limit) = 0;
}; // end struct iNetworkConnection

#endif // __iNetworkConnection_h__

