////////////////////////////////////////////////////////////////////////////////
/**
 * @file iNetworkServer.h
 * @author Christopher Case (macguyvok@gmail.com)
 * @author David Bronke (whitelynx@gmail.com)
 *
 * @brief Declaration of the network server interface.
 */
////////////////////////////////////////////////////////////////////////////////


#ifndef __iNetworkServer_h__
#define __iNetworkServer_h__

#include "csutil/scf_interface.h"

struct iNetworkAddress;
struct iNetworkConnection;
struct iNetworkPacket;
struct iNetworkPacketFactory;


/**
 * @brief The network server interface.
 *
 * This is a network server, which is able to listen for and handle multiple
 * incoming connections.
 */
struct iNetworkServer : public virtual iBase
{
public:
	SCF_INTERFACE(iNetworkServer, 1, 0, 0);

	/**
	 * Set the packet factory. This object will be used to create all packet
	 * objects.
	 */
	virtual void SetPacketFactory (iNetworkPacketFactory* factory) = 0;

	/**
	 * Listen for incoming connections on the given address.
	 */
	virtual void Listen (iNetworkAddress* address) = 0;

	/**
	 * Accept and return the next incoming connection.
	 */
	virtual csPtr<iNetworkConnection> GetIncomingConnection (bool blocking = false) = 0;

	/**
	 * Send a packet to all connected clients.
	 */
	virtual void Broadcast (iNetworkPacket* packet) = 0;

	/**
	 * Close all open connections.
	 */
	virtual void DisconnectAll () = 0;
}; // end struct iNetworkServer

#endif // __iNetworkServer_h__

