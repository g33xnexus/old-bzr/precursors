////////////////////////////////////////////////////////////////////////////////
/**
 * @file iNetworkPacketFactory.h
 * @author Christopher Case (macguyvok@gmail.com)
 * @author David Bronke (whitelynx@gmail.com)
 *
 * @brief Declaration of the network packet factory interface.
 */
////////////////////////////////////////////////////////////////////////////////


#ifndef __iNetworkPacketFactory_h__
#define __iNetworkPacketFactory_h__

#include "csutil/scf_interface.h"

struct iDataBuffer;
struct iNetworkPacket;


/**
 * @brief A factory for producing packets.
 *
 * An interface to construct network packets.
 */
struct iNetworkPacketFactory : public virtual iBase
{
public:
	SCF_INTERFACE(iNetworkPacketFactory, 1, 0, 0);

	/**
	 * Create an iNetworkPacket with the given contents.
	 */
	virtual csPtr<iNetworkPacket> createPacket (iDataBuffer* buffer) = 0;
}; // end struct iNetworkPacketFactory

#endif // __iNetworkPacketFactory_h__

