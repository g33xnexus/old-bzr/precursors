////////////////////////////////////////////////////////////////////////////////
/**
 * @file iNetworkPacket.h
 * @author Christopher Case (macguyvok@gmail.com)
 * @author David Bronke (whitelynx@gmail.com)
 *
 * @brief Declaration of the network packet interface.
 */
////////////////////////////////////////////////////////////////////////////////


#ifndef __iNetworkPacket_h__
#define __iNetworkPacket_h__

#include "csutil/scf_interface.h"

struct iDataBuffer;


/**
 * @brief A packet of information being transferred over the network.
 *
 * This is a packet being sent to or received from the network.
 */
struct iNetworkPacket : public virtual iBase
{
public:
	SCF_INTERFACE(iNetworkPacket, 1, 0, 0);

	/**
	 * Get a buffer with the contents of this packet in network byte order.
	 */
	virtual csRef<iDataBuffer> getContents () = 0;

	/**
	 * Set the contents of this packet to the given data buffer, which is
	 * expected to be in network byte order.
	 */
	virtual void setContents (iDataBuffer* buffer) = 0;
}; // end struct iNetworkPacket

#endif // __iNetworkPacket_h__

