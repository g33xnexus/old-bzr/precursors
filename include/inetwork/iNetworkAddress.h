////////////////////////////////////////////////////////////////////////////////
/**
 * @file iNetworkAddress.h
 * @author Christopher Case (macguyvok@gmail.com)
 * @author David Bronke (whitelynx@gmail.com)
 *
 * @brief Declaration of the network address interface.
 */
////////////////////////////////////////////////////////////////////////////////


#ifndef __iNetworkAddress_h__
#define __iNetworkAddress_h__

#include "csutil/scf_interface.h"

struct iString;


/**
 * @brief Address types.
 *
 * The type of address denoted by a given iNetworkAddress.
 *
 * There's plenty of other address families we could support, including IPX,
 * NetBEUI, IrDA, and Bluetooth. There's probably no need, however.
 */
enum AddressType
{
	/// Local UNIX socket address. Corresponds to AF_UNIX, AF_LOCAL, and AF_FILE
	UNIX,

	/// IPv4 network address. Corresponds to AF_INET
	IP,

	/// IPv6 network address. Corresponds to AF_INET6
	IPV6
};

/**
 * @brief A network address.
 *
 * This is an address denoting a given host on the network.
 *
 */
struct iNetworkAddress : public virtual iBase
{
public:
	SCF_INTERFACE(iNetworkAddress, 1, 0, 0);

	/**
	 * Get the type of this address.
	 */
	virtual AddressType getType () = 0;

	/**
	 * Get the host specified by this address. (returns 'localhost' for UNIX
	 * addresses)
	 */
	virtual csPtr<iString> getHost () = 0;

	/**
	 * Get the port specified by this address. (returns 0 for UNIX addresses)
	 */
	virtual unsigned int getPort () = 0;

	/**
	 * Get the path specified by this address. (returns an empty string for all
	 * non-UNIX addresses)
	 */
	virtual csPtr<iString> getPath () = 0;
}; // end struct iNetworkAddress

#endif // __iNetworkAddress_h__

