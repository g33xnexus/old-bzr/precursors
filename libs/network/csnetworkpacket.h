////////////////////////////////////////////////////////////////////////////////
/**
 * @file packet.h
 * @author Christopher Case <chris.case@g33xnexus.com>
 * @author David Bronke <david.bronke@g33xnexus.com>
 * @author Travis Odom <travis.odom@g33xnexus.com
 *
 * @brief csNetworkPacket Interface Header.
 */
////////////////////////////////////////////////////////////////////////////////


#ifndef NETWORK_ENET_PACKET_H
#define NETWORK_ENET_PACKET_H

// Precursors includes
#include "global.h"
#include "inetwork/iNetworkPacket.h"

// Crystal Space includes
#include <csutil/ref.h>
#include <csutil/scf_implementation.h>

// Forward Declarations
struct iString;
struct iKeyValuePair;

/**
 * @brief A packet of information being transferred over the network.
 *
 * This is a packet being sent to or received from the network.
 */
class csNetworkPacket :
	public scfImplementation1<csNetworkPacket, iNetworkPacket>
{
public:
	csNetworkPacket (iBase *piBase, size_t initialSize = 256);
	csNetworkPacket (iBase *piBase, iDataBuffer* buffer);

	virtual ~csNetworkPacket ();

	/*
	 * iNetworkPacket interface
	 */
public:
	/**
	 * Get a buffer with the contents of this packet in network byte order.
	 */
	csRef <iDataBuffer> getContents ();

	/**
	 * Set the contents of this packet to the given data buffer, which is
	 * expected to be in network byte order.
	 */
	void setContents (iDataBuffer* buffer);

	/*
	 * Write Functions
	 */
	
	/**
	 * Write an int8 to the packet.
	 *
	 * @param data The int8 to be written.	
	 */
	void write (int8 data);
	
	/**
	 * Write an int16 to the packet.
	 *
	 * @param data The int16 to be written. 	
	 */
	void write (int16 data);

	/**
	 * Write an int32 to the packet.
	 *
	 * @param data The int32 to be written. 	
	 */
	void write (int32 data);

	/**
	 * Write an int64 to the packet.
	 *
	 * @param data The int64 to be written. 	
	 */
	void write (int64 data);

	/**
	 * Write an uint8 to the packet.
	 *
	 * @param data The uint8 to be written.	
	 */
	void write (uint8 data);

	/**
	 * Write an uint16 to the packet.
	 *
	 * @param data The uint16 to be written.	
	 */
	void write (uint16 data);

	/**
	 * Write an uint32 to the packet.
	 *
	 * @param data The uint32 to be written.	
	 */
	void write (uint32 data);

	/**
	 * Write an uint64 to the packet.
	 *
	 * @param data The uint64 to be written.	
	 */
	void write (uint64 data);

	/**
	 * Write an float to the packet.
	 *
	 * @param data The float to be written.	
	 */
	void write (float data);

	/**
	 * Write an double to the packet.
	 *
	 * @param data The double to be written.	
	 */
	void write (double data);

	/**
	 * Write an iKeyValuePair* to the packet.
	 *
	 * @param data The iKeyValuePair* to be written.	
	 */
	void writeKeyValuePair (iKeyValuePair* data);

	/**
	 * Write an const char* to the packet.
	 *
	 * @param data The const char* to be written.
	 * @param length The length of data. If -1, write 
	 * will use strlen to determine length
	 */
	void write (const char* data, int32 length = -1);

	/**
	 * Write an iString* to the packet.
	 *
	 * @param data The iString* to be written.	
	 */
	void write (iString* data);

	/**
	 * Write an arbitrarily-typed buffer to the packet. (WARNING: Data must
	 * be independantly endian-corrected!)
	 *
	 * @param data The char* to be written.	
	 */
	void writeBuffer (char* data, uint32 length);
	
	/* 
	 * Read Functions 
	 */
	
	/**
	 * Read back an int8 from the packet.
	 * @return The int8 being read back.
	 */
	int8 readInt8 ();

	/**
	 * Read back an int16 from the packet.
	 * @return The int16 being read back.
	 */
	int16 readInt16 ();

	/**
	 * Read back an int32 from the packet.
	 * @return The int32 being read back.
	 */
	int32 readInt32 ();

	/**
	 * Read back an int64 from the packet.
	 * @return The int64 being read back.
	 */
	int64 readInt64 ();

	/**
	 * Read back an uint8 from the packet.
	 * @return The uint8 being read back.
	 */
	uint8 readUint8 ();

	/**
	 * Read back an uint16 from the packet.
	 * @return The uint16 being read back.
	 */
	uint16 readUint16 ();

	/**
	 * Read back an uint32 from the packet.
	 * @return The uint32 being read back.
	 */
	uint32 readUint32 ();

	/**
	 * Read back an uint64 from the packet.
	 * @return The uint64 being read back.
	 */
	uint64 readUint64 ();

	/**
	 * Read back an float from the packet.
	 * @return The float being read back.
	 */
	float readFloat ();

	/**
	 * Read back an double from the packet.
	 * @return The double being read back.
	 */
	double readDouble ();

	/**
	 * Read back an iKeyValuePair from the packet.
	 * @return The iKeyValuePair being read back.
	 */
	csPtr<iKeyValuePair> readKeyValuePair ();

	/**
	 * Read back an iString from the packet.
	 * @return The iString being read back.
	 */
	csPtr<iString> readString ();

	/**
	 * Read back a null-terminated string from the packet.
	 * @param str The string being read back.
	 */
	void readString (char* &str);

	/**
	 * Read back an arbitrarily-typed buffer from the packet. (WARNING: Data must
	 * be independently endian-corrected!)
	 * @param buff The arbitrarily-typed buffer being read back.
	 * @param length The length of the arbitrarily-typed buffer
	 */
	void readBuffer (char* &buff, uint32 &length); 

private:
	/* 
	 * Private Variables 
	 */

	/**
	 * A pointer to the internal buffer
	 */
	uint8* contents;

	/**
	 * A pointer to the next unfilled position in the buffer
	 */
	uint8* nextPosition;

	/**
	 * A pointer to the next read position in the buffer
	 */
	uint8* readPosition;

	/**
	 * The number of bytes currently allocated
	 */
	size_t allocated;

	/**
	 * The number of bytes that have been filled
	 */
	size_t filled;

	/* 
	 * Private Functions
	 */

	/**
	 * Grow the buffer by double it's current size
	 */
	void grow ();

	/**
	 * Append an integer value to the internal buffer
	 * @params value The integer value to append
	 */
	void append (int value);

	/**
	 * Append arbitray data to the internal buffer
	 * @params data The data to append
	 * @params bytes The size (in bytes) of the data to append.
	 */
	void append (const void* data, size_t bytes);
	
	/**
	 * Pop arbitray data from the internal buffer
	 * @params bytes The size (in bytes) of the data to pop.
	 */
	void pop (uint8* buff, size_t bytes);
	
}; // end struct csNetworkPacket

#endif // NETWORK_ENET_PACKET_H

