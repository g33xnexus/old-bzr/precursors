////////////////////////////////////////////////////////////////////////////////
/**
 * @file packetfactory.cpp
 * @author Christopher Case <chris.case@g33xnexus.com>
 * @author Travis Odom <travis.odom@g33xnexus.com>
 *
 * @brief Implementation of the network packet factory interface.
 */
////////////////////////////////////////////////////////////////////////////////


// Precursors Includes
#include "csnetworkpacketfactory.h"
#include "csnetworkpacket.h"

SCF_IMPLEMENT_FACTORY (csNetworkPacketFactory)

////////////////////////////////////////////////////////////////////////////
// Constructor and Destructor

/// Constructor
csNetworkPacketFactory::csNetworkPacketFactory (iBase *piBase) : scfImplementationType (this, piBase)
{
} // end csNetworkPacketFactory ()

/// Destructor
csNetworkPacketFactory::~csNetworkPacketFactory ()
{
} // end ~csNetworkPacketFactory ()

////////////////////////////////////////////////////////////////////////////
// iNetworkPacket interface

csPtr<iNetworkPacket> csNetworkPacketFactory::createPacket (iDataBuffer* buffer)
{
	csPtr <iNetworkPacket> packet = new csNetworkPacket (this, buffer);
	return packet;
} // end createPacket () 

