////////////////////////////////////////////////////////////////////////////////
/**
 * @file packetfactory.h
 * @author Christopher Case <macguyvok@gmail.com>
 * @author David Bronke <whitelynx@gmail.com>
 *
 * @brief Declaration of the network packet factory interface.
 */
////////////////////////////////////////////////////////////////////////////////


#ifndef NETWORK_ENET_PACKETFACTORY_H
#define NETWORK_ENET_PACKETFACTORY_H

// Precursors includes
#include "global.h"
#include "inetwork/iNetworkPacketFactory.h"

// Crystal Space includes
#include <csutil/ref.h>
#include <csutil/scf_implementation.h>
#include <csutil/scf.h>

/**
 * @brief A factory for producing packets.
 *
 * An interface to construct network packets.
 */
class csNetworkPacketFactory :
	public scfImplementation1<csNetworkPacketFactory, iNetworkPacketFactory>
{
public:
	csNetworkPacketFactory (iBase *piBase);
	virtual ~csNetworkPacketFactory ();

	/*
	 * iNetworkPacket interface
	 */
public:
	/**
	 * Create an iNetworkPacket with the given contents.
	 */
	csPtr<iNetworkPacket> createPacket (iDataBuffer* buffer);
}; // end struct csNetworkPacketFactory

#endif // NETWORK_ENET_PACKETFACTORY_H

