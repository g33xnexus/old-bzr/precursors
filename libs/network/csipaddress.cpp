//////////////////////////////////////////////////////////////////////////////
/**
 * @file csipaddress.cpp
 * @author Katriska Francis (katriska.francis@33xnexus.com)
 *
 * @brief Declaration of the IPV4 Address interface.
 */
//////////////////////////////////////////////////////////////////////////////

// Precursors includes
#include "csipaddress.h"

// CS Includes
#include "iutil/string.h"
#include "ivaria/keyval.h"
#include "csutil/csstring.h"
#include "cstool/keyval.h"
#include <csutil/scfstr.h>
#include <csutil/databuf.h>

SCF_IMPLEMENT_FACTORY (csIPAddress)

//////////////////////////////////////////////////////////////////////////
// Constructor and Destructor

// Constructor
csIPAddress::csIPAddress (iBase *piBase, const char* host, unsigned int port) : scfImplementationType (this, piBase), ipHost(host), ipPort(port)
{
	// Set this to IP since we're working with ipv4 for this class
	ipType = IP;
} // end csIPAddress ()

// Destructor
csIPAddress::~csIPAddress ()
{
} // end ~csIPAddress()

// Get the type of this address.
AddressType csIPAddress::getType()
{
	// Ensure that we're returning IP
	ipType = IP;

	return ipType;
} // end getType()

// Get the host specified by this address. 
csPtr<iString> csIPAddress::getHost ()
{
	return (csPtr<iString>) csRef<iString> (new scfString (ipHost));
} // end getHost()

// Get the port specified by this address. 
unsigned int csIPAddress::getPort ()
{
	return ipPort;
} // end getPort()

// Get the path specified by this address.
csPtr<iString> csIPAddress::getPath ()
{
	return (csPtr<iString>) csRef<iString> (new scfString (""));
} // end getPath()
