////////////////////////////////////////////////////////////////////////////////
/**
 * @file csipaddress.h
 * @author Christopher Case (macguyvok@gmail.com)
 * @author David Bronke (whitelynx@gmail.com)
 *
 * @brief Declaration of the network address interface.
 */
////////////////////////////////////////////////////////////////////////////////


#ifndef LIBS_NETWORK_CSIPADDRESS_H
#define LIBS_NETWORK_CSIPADDRESS_H

// Precursors includes
#include "global.h"
#include "inetwork/iNetworkAddress.h"

// Crystal Space includes
#include <csutil/ref.h>
#include <csutil/scf_implementation.h>

/**
 * @brief A network IP address.
 *
 * This is an address denoting a given host on the network.
 *
 */
class csIPAddress :
	public scfImplementation1<csIPAddress, iNetworkAddress>
{
private:
	// Holds hostname or ip address 
	csString ipHost;

	// Holds the specified port
	int ipPort;

	// Holds the ip type so that we know which implimentation is being used
	AddressType ipType;

public:
	csIPAddress (iBase *piBase, const char* host = "", unsigned int port = 0);
	~csIPAddress ();

	/*
	 * iNetworkAddress interface
	 */
public:

	/**
	 * Get the type of this address.
	 */
	AddressType getType ();

	/**
	 * Get the host specified by this address. (returns 'localhost' for UNIX
	 * addresses)
	 */
	csPtr<iString> getHost ();

	/**
	 * Get the port specified by this address. (returns 0 for UNIX addresses)
	 */
	unsigned int getPort ();

	/**
	 * Get the path specified by this address. (returns an empty string for all
	 * non-UNIX addresses)
	 */
	csPtr<iString> getPath ();

}; // end class csIPAddress

#endif // LIBS_NETWORK_CSIPADDRESS_H

