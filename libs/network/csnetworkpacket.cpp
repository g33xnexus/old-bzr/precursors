////////////////////////////////////////////////////////////////////////////////
/**
 * @file packet.cpp
 * @author Christopher Case <chris.case@33xnexus.com>
 * @author Travis Odom <travis.odom@g33xnexus.com>
 *
 * @brief Implementation of csNetworkPacket.
 */
////////////////////////////////////////////////////////////////////////////////

// Precursors includes
#include "csnetworkpacket.h"

// CS Includes
#include <iutil/string.h>
#include <ivaria/keyval.h>
#include <csutil/csstring.h>
#include <cstool/keyval.h>
#include <csutil/scfstr.h>
#include <csutil/databuf.h>

SCF_IMPLEMENT_FACTORY (csNetworkPacket)


////////////////////////////////////////////////////////////////////////////
// Constructor and Destructor

/// Constructor
csNetworkPacket::csNetworkPacket (iBase *piBase, size_t initialSize)  : scfImplementationType (this, piBase)
{

	// Internal buffer initialization code
	allocated = initialSize;
	filled = 0;
	contents = new uint8[allocated];
	nextPosition = contents;
	readPosition = contents;

} // end csNetworkPacket ()

/// Constructor
csNetworkPacket::csNetworkPacket (iBase *piBase, iDataBuffer* buffer)  : scfImplementationType (this, piBase)
{
	// Set contents to null
	contents = NULL;

	// Contruct our packet with the buffer we were given.
	setContents (buffer);
} // end csNetworkPacket ()

/// Destructor
csNetworkPacket::~csNetworkPacket ()
{
} // end ~csNetworkPacket()

////////////////////////////////////////////////////////////////////////////
// iNetworkPacket interface

csRef<iDataBuffer> csNetworkPacket::getContents ()
{
	csRef<iDataBuffer> data = new CS::DataBuffer<> ((char*) contents, filled, false);
	return data;
} // end getContents ()

void csNetworkPacket::setContents (iDataBuffer* buffer)
{
	if (contents != NULL)
	{
		delete contents;
	} // end if
	
	filled = buffer->GetSize ();
	allocated = filled;
	
	contents = new uint8[allocated];	
	nextPosition = (uint8*) memcpy (contents, buffer->GetData (), buffer->GetSize ());
	readPosition = contents;
} // end setContents ()

////////////////////////////////////////////////////////////////////////////
// Write Functions

void csNetworkPacket::write (int8 data)
{
	size_t length = sizeof (int8);
	append ((void*) &data, length);
} // end write ()

void csNetworkPacket::write (int16 data)
{
	size_t length = sizeof (int16);
	append ((void*) &data, length);
} // end write ()

void csNetworkPacket::write (int32 data)
{
	size_t length = sizeof (int32);
	append ((void*) &data, length);
} // end write ()

void csNetworkPacket::write (int64 data)
{
	size_t length = sizeof (int64);
	append ((void*) &data, length);
} // end write ()

void csNetworkPacket::write (uint8 data)
{
	size_t length = sizeof (uint8);
	append ((void*) &data, length);
} // end write ()

void csNetworkPacket::write (uint16 data)
{
	size_t length = sizeof (uint16);
	append ((void*) &data, length);
} // end write ()

void csNetworkPacket::write (uint32 data)
{
	size_t length = sizeof (uint32);
	append ((void*) &data, length);
} // end write ()

void csNetworkPacket::write (uint64 data)
{
	size_t length = sizeof (uint64);
	append ((void*) &data, length);
} // end write ()

void csNetworkPacket::write (float data)
{
	size_t length = sizeof (float);
	append ((void*) &data, length);
} // end write ()

void csNetworkPacket::write (double data)
{
	size_t length = sizeof (double);
	append ((void*) &data, length);
} // end write ()

void csNetworkPacket::writeKeyValuePair (iKeyValuePair* data)
{
	write (data->GetKey (), strlen (data->GetKey ()));
	write (data->GetValue (), strlen (data->GetValue ()));
} // end writeKeyValuePair ()

void csNetworkPacket::write (const char* data, int32 length)
{
	if (length > 0)
	{
		length = strlen (data);
	} // end if 
	
	// First, we write the length of the string in the buffer.
	write (length);
	
	// Now, we write the contents
	append ((void*) data, length);
} // end write ()

void csNetworkPacket::write (iString* data)
{
	const char* sBuff = new char[data->Length ()];
	sBuff = data->GetData ();
	write (sBuff, data->Length ());
} // end write ()

void csNetworkPacket::writeBuffer (char* data, uint32 length)
{
	write (data, length);
} // end writeBuffer ()

////////////////////////////////////////////////////////////////////////////
// Read Functions 

int8 csNetworkPacket::readInt8 ()
{
	size_t length = sizeof (int8);
	int8 data;
	pop ((uint8*) &data, length);
	return data;
} // end readInt8 ()

int16 csNetworkPacket::readInt16 ()
{
	size_t length = sizeof (int16);
	int16 data;
	pop ((uint8*) &data, length);
	return data;
} // end readInt16 ()

int32 csNetworkPacket::readInt32 ()
{
	size_t length = sizeof (int32);
	int32 data;
	pop ((uint8*) &data, length);
	return data;
} // end readInt32 ()

int64 csNetworkPacket::readInt64 ()
{
	size_t length = sizeof (int64);
	int64 data;
	pop ((uint8*) &data, length);
	return data;
} // end readInt64 ()

uint8 csNetworkPacket::readUint8 ()
{
	size_t length = sizeof (uint8);
	uint8 data;
	pop ((uint8*) &data, length);
	return data;
} // end readUint8 ()

uint16 csNetworkPacket::readUint16 ()
{
	size_t length = sizeof (uint16);
	uint16 data;
	pop ((uint8*) &data, length);
	return data;
} // end readUint16 ()

uint32 csNetworkPacket::readUint32 ()
{
	size_t length = sizeof (uint32);
	uint32 data;
	pop ((uint8*) &data, length);
	return data;
} // end readUint32 ()

uint64 csNetworkPacket::readUint64 ()
{
	size_t length = sizeof (uint64);
	uint64 data;
	pop ((uint8*) &data, length);
	return data;
} // end readUint64 ()

float csNetworkPacket::readFloat ()
{
	size_t length = sizeof (float);
	float data;
	pop ((uint8*) &data, length);
	return data;
} // end readFloat ()

double csNetworkPacket::readDouble ()
{
	size_t length = sizeof (double);
	double data;
	pop ((uint8*) &data, length);
	return data;
} // end readDouble ()

csPtr<iKeyValuePair> csNetworkPacket::readKeyValuePair ()
{
	char* key;
	char* value;
	readString (key);
	readString (value);

	csPtr<iKeyValuePair> data = new csKeyValuePair (key, value);
	return data;
} // end readKeyValuePair ()

csPtr<iString> csNetworkPacket::readString ()
{
	char* sdata;
	readString (sdata);

	scfString* data = new scfString (sdata);
	delete sdata;
	return (csPtr<iString>) csRef<iString> (data);
} // end readString ()

void csNetworkPacket::readString (char* &str)
{
	uint32 length = readUint32 ();
	str = new char[length + 1];

	pop ((uint8*) str, length);
	str[length] = '\0';
} // end readString () 

void csNetworkPacket::readBuffer (char* &buff, uint32 &length)
{
	length = readUint32 ();
	buff = new char[length];

	pop ((uint8*) buff, length);
} // end readBuffer () 

////////////////////////////////////////////////////////////////////////////
// Private Functions

void csNetworkPacket::grow ()
{
	// Keep a pointer to the current contents of the buffer
	uint8* oldbuffer = contents;

	// We want to double our current size, as an optimization
	allocated *= 2;

	// Create a new contents with the requested size
	contents = (new uint8[allocated]);

	// Copy in the contents of the old contents
	nextPosition = (uint8*) mempcpy (contents, oldbuffer, filled);
	readPosition = contents;
} // end grow ()

void csNetworkPacket::append (int value)
{
	size_t bytes = sizeof (int);
	
	// We grow until we can fit the contents of what we're trying to add.
	while (filled + bytes > allocated)
	{
		grow ();
	} // end while

	// Increase filled by the size of what we're trying to add.
	filled += bytes;
	
	// Set nextPosition to the memory address of the int we're adding.
	*((int*) nextPosition) = value;
	
	// Copy the contents of what we're trying to add into the contents.
	nextPosition = ((uint8*) nextPosition) + bytes;
	readPosition = contents;
} // end append ()

void csNetworkPacket::append (const void* data, size_t bytes)
{
	// We grow until we can fit the contents of what we're trying to add.
	while (filled + bytes > allocated)
	{
		grow ();
	} // end while

	// Increase filled by the size of what we're trying to add.
	filled += bytes;

	// Copy the contents of what we're trying to add into the contents.
	nextPosition = (uint8*) mempcpy (nextPosition, data, bytes);
	readPosition = contents;
} // end append ()

void csNetworkPacket::pop (uint8* buff, size_t bytes)
{
	memcpy (buff, readPosition, bytes);
	readPosition += bytes;
} // end pop ()
