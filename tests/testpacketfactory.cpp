// External Includes
#include <UnitTest++.h>
#include <string.h>
#include <limits.h>
#include <math.h>

// Precursors Includes
#include "../libs/network/csnetworkpacket.h"
#include "../libs/network/csnetworkpacketfactory.h"

// CrystalSpace Includes
#include <csutil/databuf.h>
#include <iutil/string.h>
#include <ivaria/keyval.h>
#include <csutil/csstring.h>
#include <cstool/keyval.h>
#include <csutil/scfstr.h>

SUITE (PacketTest)
{
	TEST (GetSetContents)
	{
		// Make a DataBuffer with the string 'test123'
		std::string testString = "test123";
		csRef<iDataBuffer> test = new CS::DataBuffer<> ((char*) testString.c_str(), 128);
		
		// Make a packet factory
		csRef<iNetworkPacketFactory> packetfact = new csNetworkPacketFactory (test);

		// Make a packet from the factory
		csRef<iNetworkPacket> packet = packetfact->createPacket (test);

		// set the contents of the packet, and then read them back out.
		packet->setContents (test);
		csRef<iDataBuffer> test2 = new CS::DataBuffer<> (packet->getContents());
		
		// Compare the equality of the two strings
		std::string testString2 = test2->GetData ();
		CHECK_EQUAL (testString.c_str (), testString2.c_str ());
	}
}
