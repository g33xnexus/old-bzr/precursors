// External Includes
#include <UnitTest++.h>
#include <string.h>
#include <limits.h>
#include <math.h>

// Precursors Includes
#include "../libs/network/csnetworkpacket.h"

// CrystalSpace Includes
#include <csutil/databuf.h>
#include <iutil/string.h>
#include <ivaria/keyval.h>
#include <csutil/csstring.h>
#include <cstool/keyval.h>
#include <csutil/scfstr.h>

SUITE (PacketTest)
{
	TEST (GetSetContents)
	{
		// Make a DataBuffer with the string 'test123'
		std::string testString = "test123";
		csRef<iDataBuffer> test = new CS::DataBuffer<> ((char*) testString.c_str(), 128);
		
		// Make a packet
		csRef<iNetworkPacket> packet = new csNetworkPacket (test, 128);

		// set the contents of the packet, and then read them back out.
		packet->setContents (test);
		csRef<iDataBuffer> test2 = new CS::DataBuffer<> (packet->getContents());
		
		// Compare the equality of the two strings
		std::string testString2 = test2->GetData ();
		CHECK_EQUAL (testString.c_str (), testString2.c_str ());
	}

	TEST (ConstructorContents)
	{
		// Make a DataBuffer with the string 'test123'
		std::string testString = "test123";
		csRef<iDataBuffer> test = new CS::DataBuffer<> ((char*) testString.c_str(), 128);
		
		// Make a packet
		csRef<iNetworkPacket> packet = new csNetworkPacket (test, test);

		// set the contents of the packet, and then read them back out.
		csRef<iDataBuffer> test2 = new CS::DataBuffer<> (packet->getContents());
		
		// Compare the equality of the two strings
		std::string testString2 = test2->GetData ();
		CHECK_EQUAL (testString.c_str (), testString2.c_str ());
		
	}

	struct EmptyPacket
	{
		EmptyPacket ()
		{
			packet = new csNetworkPacket (NULL, 128);
		}

		~EmptyPacket ()
		{
		}

		csRef<csNetworkPacket> packet;
	};

#define INT_TEST_PATTERN(datatype,method,val1,val2,val3,val4) \
		datatype testNumber1 = val1;\
		datatype testNumber2 = val2;\
		datatype testNumber3 = val3;\
		datatype testNumber4 = val4;\
		datatype result;\
\
		/* Write the numbers to the packet */\
		packet->write (testNumber1);\
		packet->write (testNumber2);\
		packet->write (testNumber3);\
		packet->write (testNumber4);\
\
		/* Read back and test the written numbers */\
		result = packet->method ();\
		CHECK_EQUAL (testNumber1, result);\
\
		result = packet->method ();\
		CHECK_EQUAL (testNumber2, result);\
\
		result = packet->method ();\
		CHECK_EQUAL (testNumber3, result);\
\
		result = packet->method ();\
		CHECK_EQUAL (testNumber4, result);

	TEST_FIXTURE(EmptyPacket, ReadWriteInt8)
	{
		INT_TEST_PATTERN(int8,readInt8,2,103,CHAR_MIN,CHAR_MAX)
	}

	TEST_FIXTURE(EmptyPacket, ReadWriteInt16)
	{
		INT_TEST_PATTERN(int16,readInt16,3,293,SHRT_MIN,SHRT_MAX)
	}

	TEST_FIXTURE(EmptyPacket, ReadWriteInt32)
	{
		INT_TEST_PATTERN(int32,readInt32,1,2293,INT_MIN,INT_MAX)
	}

	TEST_FIXTURE(EmptyPacket, ReadWriteInt64)
	{
		INT_TEST_PATTERN(int64,readInt64,4,229324,LONG_MIN,LONG_MAX)
	}

	TEST_FIXTURE(EmptyPacket, ReadWriteUint8)
	{
		INT_TEST_PATTERN(uint8,readUint8,2,103,233,UCHAR_MAX)
	}

	TEST_FIXTURE(EmptyPacket, ReadWriteUint16)
	{
		INT_TEST_PATTERN(uint16,readUint16,3,293,9998,USHRT_MAX)
	}

	TEST_FIXTURE(EmptyPacket, ReadWriteUint32)
	{
		INT_TEST_PATTERN(uint32,readUint32,1,2293,994298,UINT_MAX)
	}

	TEST_FIXTURE(EmptyPacket, ReadWriteUint64)
	{
		INT_TEST_PATTERN(uint64,readUint64,4,229324,99214982,ULONG_MAX)
	}

#define FLOAT_TEST_PATTERN(datatype,method,val1,val2,val3,val4,val5) \
		datatype testNumber1 = val1;\
		datatype testNumber2 = val2;\
		datatype testNumber3 = val3;\
		datatype testNumber4 = val4;\
		datatype testNumber5 = val5;\
		datatype result;\
\
		/* Write the numbers to the packet */\
		packet->write (testNumber1);\
		packet->write (testNumber2);\
		packet->write (testNumber3);\
		packet->write (testNumber4);\
		packet->write (testNumber5);\
\
		/* Read back and test the written numbers */\
		result = packet->method ();\
		CHECK_CLOSE (testNumber1, result, 0.0001);\
\
		result = packet->method ();\
		CHECK_CLOSE (testNumber2, result, 0.0001);\
\
		result = packet->method ();\
		CHECK_CLOSE (testNumber3, result, 0.0001);\
\
		result = packet->method ();\
		CHECK_CLOSE (testNumber4, result, 0.0001);\
\
		result = packet->method ();\
		CHECK_CLOSE (testNumber5, result, 0.0001);

	TEST_FIXTURE(EmptyPacket, ReadWriteFloat)
	{
		FLOAT_TEST_PATTERN(float,readFloat,0,HUGE_VALF,M_E,M_PI,INFINITY)
	}

	TEST_FIXTURE(EmptyPacket, ReadWriteDouble)
	{
		FLOAT_TEST_PATTERN(double,readDouble,0,HUGE_VAL,M_E,M_PI,INFINITY)
	}

	TEST_FIXTURE(EmptyPacket, ReadWriteString)
	{
		csRef<iString> writeTest1 = new scfString ("BlahBlahblAhBlaH!");
		const char* writeTest2 = "blehblehblehblehblehblehblehblehblehblehbleh";

		packet->write (writeTest1);
		packet->write (writeTest2, strlen (writeTest2));

		csRef<iString> readTest1 = packet->readString ();
		CHECK_EQUAL (writeTest1->GetData (), readTest1->GetData ());

		char* readTest2;
		packet->readString (readTest2);
		CHECK_EQUAL (writeTest2, readTest2);
		delete readTest2;
	}
}
