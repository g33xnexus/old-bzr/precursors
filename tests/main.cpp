#include <UnitTest++.h>
#include <cssysdef.h>
#include "cstool/csapplicationframework.h"

CS_IMPLEMENT_APPLICATION

int main()
{
	return UnitTest::RunAllTests();
}
