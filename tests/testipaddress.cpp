// External Includes
#include <UnitTest++.h>
#include <string.h>
#include <limits.h>
#include <math.h>

// Precursors Includes
#include "../libs/network/csipaddress.h"

// CrystalSpace Includes
#include <iutil/string.h>

SUITE (IPAddressTest)
{
	TEST (TestIPAddress)
	{
		// Create an ip address from the given information
		csRef<iNetworkAddress> address = new csIPAddress (NULL, "MyHost", 244);
		
		// Check where ipHost and ipPort were set with the construtor
		unsigned int port = 244;
		CHECK_EQUAL (address->getPort (), port);
		
		csRef<iString> hostIP = address-> getHost ();
		CHECK_EQUAL (hostIP-> GetData(), "MyHost");
		
		// Check if the ipType is set 
		CHECK_EQUAL (address->getType (), IP);
		
		csRef<iString> pathIP = address->getPath ();
		CHECK_EQUAL(pathIP->GetData (), "");


	}
}
