srcdir = '.'
blddir = 'build'

cslibs_includepath = None
cslibs_libpath = None

CRYSTAL_VERSION = "1.3"
CEL_VERSION = "1.3"

def set_options (opt):
	opt.tool_options ('compiler_cxx')

	opt.add_option ('--with-crystal',
		dest = 'crystal_path',
		help = 'Specify the location of Crystal Space',
		metavar = 'DIR')

	opt.add_option ('--with-cel',
		dest = 'cel_path',
		help = 'Specify the location of CEL',
		metavar = 'DIR')

	opt.add_option ('--with-cegui',
		dest = 'cegui_path',
		help = 'Specify the location of CEGUI',
		metavar = 'DIR')

	opt.add_option ('--with-cs-win32libs',
		dest = 'cslibs_path',
		help = 'Specify the location of cs-win32libs',
		metavar = 'DIR')

	opt.add_option ('--with-enet',
		dest = 'enet_path',
		help = 'Specify the location of ENet',
		metavar = 'DIR')

	opt.add_option ('--with-unittest++',
		dest = 'utxx_path',
		help = 'Specify the location of libUnitTest++',
		metavar = 'DIR')

def configure (conf):
	import os.path

	conf.check_tool ('compiler_cxx')
	conf.check_tool ('checks')

	conf.checkEndian ()

	check_crystalspace (conf)
	check_cel (conf)
	check_cslibs (conf)
	check_cegui (conf)
	check_enet (conf)
	check_unittestxx (conf)

	conf.env['CPPPATH'] += [os.path.join (os.path.abspath ('.'), 'include')]

	conf.write_config_header ('config.h')

def build (bld):
	bld.add_subdirs ('src')
	bld.add_subdirs ('libs')
	#bld.add_subdirs ('plugins')
	bld.add_subdirs ('tests')

def shutdown ():
	import UnitTest, os

	unittest = UnitTest.unit_test ()
	unittest.want_to_see_test_output = 1	# We want to see the output from the tests
	unittest.run_if_waf_does = "build"		# Run unit tests whenever we build
	unittest.run ()
	unittest.print_results ()

	os.system ('cp build/default/src/precursors* .')


std_includepath = [
	'/usr/include',
	'/usr/local/include'
]
std_libpath = [
	'/usr/lib',
	'/usr/local/lib'
]

def check_crystalspace (conf):
	"Check for Crystal Space"

	import os, os.path, Params, checks
	global CRYSTAL_VERSION

	if Params.g_options.crystal_path is not None:
		CRYSTAL = Params.g_options.crystal_path
	else:
		CRYSTAL = os.getenv ('CRYSTAL')

	# Skip cs-config on windows, since it's a bit broken there.
	result = {}
	if checks.detect_platform (None) != "win32":
		# Find cs_config
		cs_config = 'cs-config'
		if CRYSTAL is not None:
			if os.path.exists (os.path.join (CRYSTAL, cs_config)):
				cs_config = os.path.join (CRYSTAL, cs_config)
			elif os.path.exists (os.path.join (CRYSTAL, 'bin', cs_config)):
				cs_config = os.path.join (CRYSTAL, 'bin', cs_config)

		crystalspace = conf.create_cfgtool_configurator ()
		crystalspace.name = "crystalspace"
		crystalspace.binary = cs_config
		crystalspace.message = "This project requires Crystal Space."
		crystalspace.define = "CRYSTAL_AVAILABLE"
		crystalspace.uselib_store = "CRYSTAL"
		crystalspace.tests['--libdir'] = 'LIBPATH'
		crystalspace.tests['--includedir'] = 'CPPPATH'
		crystalspace.tests['--libs'] = 'LINKFLAGS'
		crystalspace.tests['--cflags'] = 'CCFLAGS'
		crystalspace.tests['--cxxflags'] = 'CXXFLAGS'
		#crystalspace.tests['--available-libs'] = '' #TODO: What the hell do we do with this?
		#crystalspace.tests['--staticdeps'] = 'STATICLIB' #FIXME: This breaks shit.
		result = crystalspace.run ()

	if result == {}:
		cs_includepath = list (std_includepath)
		cs_libpath = list (std_libpath)
		if CRYSTAL is not None:
			cs_includepath.append (os.path.join (CRYSTAL, "include"))
			cs_libpath.append (CRYSTAL)
			cs_libpath.append (os.path.join (CRYSTAL, "lib"))

		# Find CS headers
		cs_header = conf.create_header_configurator ()
		cs_header.name = 'crystalspace.h'
		cs_header.path = cs_includepath
		cs_header.message = 'This project requires Crystal Space.'
		cs_header.uselib_store = "CRYSTAL"
		cs_header.mandatory = 1
		cs_header.run ()

		# Find CS libraries
		# - try without version
		cs_library = conf.create_library_configurator ()
		cs_library.path = cs_libpath
		cs_library.message = 'This project requires Crystal Space.'
		cs_library.define = "CRYSTAL_AVAILABLE"
		cs_library.uselib_store = "CRYSTAL"
		cs_library.name = 'crystalspace'
		result = cs_library.run ()

		if result != {}:
			cs_library.name = 'crystalspace_opengl'
			cs_library.mandatory = 1
			cs_library.run ()

		else:
			# - try with version
			cs_library.name = 'crystalspace-' + CRYSTAL_VERSION
			result = cs_library.run ()

			if result != {}:
				cs_library.name = 'crystalspace_opengl-' + CRYSTAL_VERSION
				cs_library.mandatory = 1
				cs_library.run ()

			else:
				# - try with -gcc (needed for mingw
				cs_library.name = 'crystalspace-' + CRYSTAL_VERSION + '-gcc'
				cs_library.mandatory = 1					# Fail if we can't find it after this.
				cs_library.env['shlib_PATTERN'] = '%s.dll'	# Make sure we're looking for the right file under mingw.
				cs_library.run ()

				cs_library.name = 'crystalspace_opengl-' + CRYSTAL_VERSION + '-gcc'
				cs_library.run ()

def check_cel (conf):
	"Check for CEL"

	import os, os.path, Params, checks
	global CEL_VERSION

	if Params.g_options.cel_path is not None:
		CEL = Params.g_options.cel_path
	else:
		CEL = os.getenv ('CEL')

	# Skip cel-config on windows, since it's a bit broken there.
	result = {}
	if checks.detect_platform (None) != "win32":
		# Find cel_config
		cel_config = 'cel-config'
		if CEL is not None:
			conf.env['LIBPATH'] += [CEL] # Apparently cel-config doesn't tell us a libdir at all.
			if os.path.exists (os.path.join (CEL, cel_config)):
				cel_config = os.path.join (CEL, cel_config)
			elif os.path.exists (os.path.join (CEL, 'bin', cel_config)):
				cel_config = os.path.join (CEL, 'bin', cel_config)

		cel = conf.create_cfgtool_configurator ()
		cel.name = "cel"
		cel.binary = cel_config
		cel.message = "This project requires CEL."
		cel.define = "CEL_AVAILABLE"
		cel.uselib_store = "CEL"
		cel.tests['--libdir'] = 'LIBPATH'
		cel.tests['--includedir'] = 'CPPPATH'
		cel.tests['--lflags'] = 'LINKFLAGS'
		cel.tests['--cflags'] = 'CCFLAGS'
		cel.tests['--cxxflags'] = 'CXXFLAGS'
		#cel.tests['--available-libs'] = '' #TODO: What the hell do we do with this?
		#cel.tests['--staticdeps'] = 'STATICLIB' #FIXME: This breaks shit.
		result = cel.run ()

	if result == {}:
		cel_includepath = list (std_includepath)
		cel_libpath = list (std_libpath)
		if CEL is not None:
			cel_includepath.append (os.path.join (CEL, "include"))
			cel_libpath.append (CEL)
			cel_libpath.append (os.path.join (CEL, "lib"))

		# Find CS headers
		cel_header = conf.create_header_configurator ()
		cel_header.name = 'celversion.h'
		cel_header.path = cel_includepath
		cel_header.message = 'This project requires CEL.'
		cel_header.uselib = "CRYSTAL"
		cel_header.uselib_store = "CEL"
		cel_header.mandatory = 1
		cel_header.run ()

		# Find CS libraries
		# - try without version
		cel_library = conf.create_library_configurator ()
		cel_library.path = cel_libpath
		cel_library.message = 'This project requires CEL.'
		cel_library.define = "CEL_AVAILABLE"
		cel_library.uselib = "CRYSTAL"
		cel_library.uselib_store = "CEL"
		cel_library.name = 'celtool'
		result = cel_library.run ()

		if result == {}:
			# - try with version
			cel_library.name = 'celtool-' + CEL_VERSION
			result = cel_library.run ()

			if result == {}:
				# - try with -gcc (needed for mingw
				cel_library.name = 'celtool-' + CEL_VERSION + '-gcc'
				cel_library.mandatory = 1					# Fail if we can't find it after this.
				cel_library.env['shlib_PATTERN'] = '%s.dll'	# Make sure we're looking for the right file under mingw.
				cel_library.run ()

def check_cslibs (conf):
	"Check for cs-win32libs"

	import os, os.path, Params
	global cslibs_includepath, cslibs_libpath

	if Params.g_options.cslibs_path is not None:
		CSLIBS = Params.g_options.cslibs_path
	else:
		CSLIBS = os.getenv ('CSLIBS')

	if CSLIBS is not None:
		cslibs_includepath = os.path.join (CSLIBS, 'common', 'include')
		if conf.env["COMPILER_CXX"] == "msvc":
			cslibs_libpath = os.path.join (CSLIBS, 'vc', 'lib')
		else:
			cslibs_libpath = os.path.join (CSLIBS, 'mingw', 'lib')

def check_cegui (conf):
	"Check for CEGUI"

	import os, os.path, Params
	global cslibs_includepath, cslibs_libpath

	if Params.g_options.cegui_path is not None:
		CEGUI = Params.g_options.cegui_path
	else:
		CEGUI = os.getenv ('CEGUI')

	cegui_pkgconfig = conf.create_pkgconfig_configurator ()
	cegui_pkgconfig.name = "CEGUI"
	cegui_pkgconfig.version = "0.5.0"
	cegui_pkgconfig.uselib_store = "CEGUI"
	if CEGUI is not None:
		cegui_pkgconfig.path = CEGUI
	result = cegui_pkgconfig.run ()

	if result == {}:
		cegui_includepath = list (std_includepath)
		cegui_libpath = list (std_libpath)
		if CEGUI is not None:
			cegui_includepath.append (CEGUI)
			cegui_includepath.append (os.path.join (CEGUI, 'include'))
			cegui_libpath.append (CEGUI)
			cegui_libpath.append (os.path.join (CEGUI, 'lib'))

		if cslibs_includepath is not None:
			cegui_includepath.append (cslibs_includepath)
		if cslibs_libpath is not None:
			cegui_libpath.append (cslibs_libpath)

		cegui_header = conf.create_header_configurator ()
		cegui_header.name = 'CEGUI.h'
		cegui_header.path = cegui_includepath
		cegui_header.mandatory = 1
		cegui_header.message = 'This project requires CEGUI.'
		cegui_header.uselib_store = "CEGUI"
		cegui_header.run ()

		cegui_library = conf.create_library_configurator ()
		cegui_library.name = 'CEGUIBase'
		cegui_library.path = cegui_libpath
		cegui_library.mandatory = 1
		cegui_library.message = 'This project requires CEGUI.'
		cegui_library.uselib_store = "CEGUI"
		cegui_library.run ()

def check_enet (conf):
	"Check for ENet"

	import os, os.path, Params

	if Params.g_options.enet_path is not None:
		ENET = Params.g_options.enet_path
	else:
		ENET = os.getenv ('ENET')

	enet_includepath = list (std_includepath)
	enet_libpath = list (std_libpath)
	if ENET is not None:
		enet_includepath.append (ENET)
		enet_includepath.append (os.path.join (ENET, 'include'))
		enet_libpath.append (ENET)

	enet_header = conf.create_header_configurator ()
	enet_header.name = 'enet/enet.h'
	enet_header.path = enet_includepath
	enet_header.mandatory = 1
	enet_header.message = 'This project requires ENet.'
	enet_header.uselib_store = "ENET"
	enet_header.run ()

	enet_library = conf.create_library_configurator ()
	enet_library.name = 'enet'
	enet_library.path = enet_libpath
	enet_library.mandatory = 1
	enet_library.message = 'This project requires ENet.'
	enet_library.uselib_store = "ENET"
	enet_library.run ()

def check_unittestxx (conf):
	"Check for UnitTest++"

	import os, os.path, Params

	if Params.g_options.utxx_path is not None:
		UNITTESTXX = Params.g_options.utxx_path
	else:
		UNITTESTXX = os.getenv ('UNITTESTXX')

	utxx_includepath = list (std_includepath)
	utxx_libpath = list (std_libpath)
	if UNITTESTXX is not None:
		utxx_includepath.append (UNITTESTXX)
		utxx_includepath.append (os.path.join (UNITTESTXX, 'src'))
		utxx_libpath.append (UNITTESTXX)

	utxx_header = conf.create_header_configurator ()
	utxx_header.name = 'UnitTest++.h'
	utxx_header.path = utxx_includepath
	utxx_header.mandatory = 1
	utxx_header.message = 'This project requires UnitTest++.'
	utxx_header.uselib_store = "UNITTESTXX"
	utxx_header.run ()

	utxx_library = conf.create_library_configurator ()
	utxx_library.name = 'UnitTest++'
	utxx_library.path = utxx_libpath
	utxx_library.mandatory = 1
	utxx_library.message = 'This project requires UnitTest++.'
	utxx_library.uselib_store = "UNITTESTXX"
	utxx_library.run ()

