////////////////////////////////////////////////////////////////////////////////
/**
 * Copyright (C) 2004-2006 G33X Nexus Entertainment (http://gne.g33xnexus.com/)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////
/**
 * @file	preClient.h
 * @author	Christophers S. Case
 * @email	chris.case@g33xnexus.com
 *
 * @brief  Declaration of preClient, the main application class.
 * 
 */
/////////////////////////////////////////////////////////////////////////////////*


#ifndef __preClient_h
#define __preClient_h

#include "global.h"
#include "csutil/csbaseeventh.h"
#include "csutil/csstring.h"
#include "cstool/csapplicationframework.h"

#include "ivideo/graph3d.h"
#include "imap/loader.h"

// CEL Includes
#include "physicallayer/entity.h"
#include "physicallayer/pl.h"
#include "behaviourlayer/bl.h"

// Forward declarations
struct iObjectRegistry;
struct iEvent;
struct iVFS;
struct iEngine;
struct iGraphics3D;
struct iCelPlLayer;
struct iCelBlLayer;
struct iCelEntity;
struct iCelRegion;
struct iDynamics;
struct iEntityLoader;
struct iKeyValuePair;
struct iMouseDriver;
struct iCollideSystem;
struct iPcCamera;
struct iPcCommandInput;
struct iCEGUI;

//struct iPcNetworkServer;
//struct iPcNetworkClient;
//struct iPreContentManager;
struct iPreWindowManager;

namespace Precursors
{
	class WindowManager;

	////////////////////////////////////////////////////////////////////////////
	/**
	 * @brief The main application class.  Manages everything for the game.
	 *
	 * This class implements the csApplicationFramework and csBaseEventHandler
	 * classes to maintain all the data for the game.
	 */
	class preClient : public csApplicationFramework, public csBaseEventHandler
	{
	private:
		////////////////////////////////////////////////////////////////////////
		// Member variables.

		/// A reference to the 2D renderer plugin.
		csWeakRef<iGraphics2D> g2d;

		/// A reference to the 3D renderer plugin.
		csWeakRef<iGraphics3D> g3d;

		/// A reference to the 3D engine plugin.
		csWeakRef<iEngine> engine;

		/// A pointer to the map loader plugin.
		csWeakRef<iLoader> loader;

		/// A pointer to vfs.
		csRef<iVFS> vfs;

		/// The mouse driver.
		csWeakRef<iMouseDriver> mouse_driver;

		/// The output console.
		csWeakRef<iConsoleOutput> conout;

		/// Collision detection.
		csWeakRef<iCollideSystem> cdsys;

		/// The application's configuration.
		csWeakRef<iConfigManager> applicationConfig;

		/// A pointer to the physical layer (CEL).
		csWeakRef<iCelPlLayer> pl;

		/// The C++ Behaviour layer
		csRef<iCelBlLayer> blcpp;

		/// The Python Behaviour Layer
		csWeakRef<iCelBlLayer> blpython;

		/// The content loader.
		//csWeakRef<iPreContentManager> contentManager;

		/// The window manager.
		csWeakRef<iPreWindowManager> windowManager;

		/// The dynamics plugin.
		csWeakRef<iDynamics> dynamics;

		/// The CEGUI plugin.
		csWeakRef<iCEGUI> cegui;

//		bool canvas_exposed;

		/// The prefix for the configuration keys
		csString cfgkeyprefix;


	private:
		////////////////////////////////////////////////////////////////////////
		// Initialization functions.

		/**
		 * Process all mounts in the application config. Returns false
		 * on error (error already reported).
		 */
		bool ProcessMounts ();

		////////////////////////////////////////////////////////////////////////
		// Event Handlers

		/**
		 * Post process frame. (2D overlays)
		 */
		virtual void PostProcessFrame ();

		/**
		 * Finally, render the screen.  This routine is called from the event
		 * handler in response to a cscmdFinalProcess broadcast message.
		 */
		virtual void FinishFrame ();

		/**
		 * Handle any broadcast events. This is used for displaying our custom
		 * command-line help.
		 */
		//virtual bool OnUnhandledEvent (iEvent& event);

		/**
		 * Handle Keyboard Events
		 * TODO: Remove this, eventually, in favor of OnUnhandledEvent
		 */
		virtual	bool OnKeyboard(iEvent& ev);

		/**
		 * Final cleanup.
		 */
		virtual void OnExit ();


	public:
		////////////////////////////////////////////////////////////////////////
		/**
		 * Constructor.
		 */
		preClient ();

		/**
		 * Destructor.
		 */
		virtual ~preClient ();

		////////////////////////////////////////////////////////////////////////
		// Initialization functions.

		/**
		 * Initializes the application - loads the plugins, the object registry,
		 * and initializes the event queue.
		 *
		 * @param argc	The number of arguments
		 * @param argv	The values of the arguments
		 */
		virtual bool OnInitialize(int argc, char* argv[]);

		/**
		 * Run the application.  Performs additional initialization (if needed),
		 * and then fires up the main run/event loop.  The loop will fire events 
		 * which actually causes Crystal Space to "run".  Only when the program 
		 * exits does this function return.
		 */
		virtual bool Application ();

		////////////////////////////////////////////////////////////////////////
		// Event Handler Setup

		/*
		 * Declare the name of this event handler.
		 */
		CS_EVENTHANDLER_NAMES("application.preClient")
	
		/*
		 * Declare that we're not terribly interested in having events
		 * delivered to us before or after other modules, plugins, etc.
		 */
		CS_EVENTHANDLER_NIL_CONSTRAINTS
	}; // end class preClient
} // end namespace Precursors

#endif // __preClient_h
