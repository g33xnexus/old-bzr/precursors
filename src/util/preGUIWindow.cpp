////////////////////////////////////////////////////////////////////////////////
/**
 * Copyright (C) 2004-2006 G33X Nexus Entertainment (http://gne.g33xnexus.com/)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////
/**
 * @file preGUIWindow.cpp
 *
 * @author David H. Bronke <David.Bronke@g33xnexus.com>
 *
 * @brief Definition of preGUIWindow, the Precursors GUI window base class.
 */
////////////////////////////////////////////////////////////////////////////////

// Precursors Includes
#include "global.h"

// CEGUI Includes
#include "CEGUI.h"
#include "CEGUIInputEvent.h"

// Precursors Includes
#include "iwindowmanager/preGUIWindow.h"

// CS Includes
#include "cssysdef.h"
#include "ivaria/reporter.h"
#include "ivaria/icegui.h"
#include "csutil/csevcord.h"
#include "csutil/eventnames.h"
#include "iutil/pluginconfig.h"


////////////////////////////////////////////////////////////////////////
/// Constructors and destructors
/// @{

/// Constructor.
preGUIWindow::preGUIWindow (iObjectRegistry* object_reg)
	: scfImplementationType (this)
{
	preGUIWindow::object_reg = object_reg;

	windowManager = csQueryRegistry<iPreWindowManager> (object_reg);
	if (!windowManager)
	{
		csReport (object_reg, CS_REPORTER_SEVERITY_ERROR, "precursors.addon.contentloader", "Can't find the Precursors window manager!");
	} // end if

	cegui = csQueryRegistry<iCEGUI> (object_reg);
	if (!cegui)
	{
		csReport (object_reg, CS_REPORTER_SEVERITY_ERROR, "precursors.addon.contentloader", "Can't find the CEGUI plugin!");
	} // end if

	// Set the single click timeout to infinity
	cegui->GetSystemPtr ()->setSingleClickTimeout (100000000.0);

	ceguiWinMgr = cegui->GetWindowManagerPtr ();
} // end preGUIWindow ()

/// Constructor.
preGUIWindow::preGUIWindow (iObjectRegistry* object_reg, const char* windowPath)
	: scfImplementationType (this)
{
	preGUIWindow::object_reg = object_reg;

	windowManager = csQueryRegistry<iPreWindowManager> (object_reg);
	if (!windowManager)
	{
		csReport (object_reg, CS_REPORTER_SEVERITY_ERROR, "precursors.addon.contentloader", "Can't find the Precursors window manager!");
	} // end if

	csRef<iCEGUI> cegui = csQueryRegistry<iCEGUI> (object_reg);
	if (!cegui)
	{
		csReport (object_reg, CS_REPORTER_SEVERITY_ERROR, "precursors.addon.contentloader", "Can't find the CEGUI plugin!");
	} // end if

	// Set the single click timeout to infinity
	cegui->GetSystemPtr ()->setSingleClickTimeout (100000000.0);

	window = windowManager->LoadCEGUILayout (windowPath);

	// Subscribe to all keypress events in this window
	keyboardCon = window->subscribeEvent (CEGUI::Window::EventKeyDown,
		CEGUI::Event::Subscriber (&preGUIWindow::OnKeyPress, this));

	// Subscribe to all mouse click events in this window
	mouseCon = window->subscribeEvent (CEGUI::Window::EventMouseClick,
		CEGUI::Event::Subscriber (&preGUIWindow::OnClicked, this));

	ceguiWinMgr = cegui->GetWindowManagerPtr ();
} // end preGUIWindow ()

/// Destructor.
preGUIWindow::~preGUIWindow ()
{
} // end ~preGUIWindow ()

/// @}


////////////////////////////////////////////////////////////////////////
/// Window Management Methods
/// @{

/**
 * Hide this window.
 */
void preGUIWindow::Hide ()
{
	window->setVisible (false);
	window->deactivate ();
} // end Hide ()

/**
 * Display this window.
 */
void preGUIWindow::Show ()
{
	window->setVisible (true);
	window->activate ();
} // end Show ()

/**
 * Get a pointer to the underlying CEGUI window.
 */
CEGUI::Window* preGUIWindow::GetWindow ()
{
	return window;
} // end GetWindow ()

/**
 * Toggle the visibility of this window.
 * @return true if the window is now visible, false otherwise.
 */
bool preGUIWindow::Toggle ()
{
	if (window->isVisible ())
	{
		window->setVisible (false);
		window->activate ();
		return false;
	}
	else
	{
		window->setVisible (true);
		window->deactivate ();
		return true;
	} // end if
} // end Toggle ()

/**
 * Send this window a message.
 * @return true on success, false otherwise.
 */
bool preGUIWindow::SendMessage (const char* message, csHash<csVariant, csString> parameters)
{
	return false;
} // end SendMessage ()

/// @}


////////////////////////////////////////////////////////////////////////
/// Event Handlers
/// @{

/// Called when the window is clicked.
bool preGUIWindow::OnClicked (const CEGUI::EventArgs& args)
{
	CEGUI::Window* window =
		static_cast <const CEGUI::WindowEventArgs&> (args).window;
	CEGUI::Point position =
		static_cast <const CEGUI::MouseEventArgs&> (args).position;

	CEGUI::Window* child = window->getChildAtPosition (position);

	// Only handle click events for Buttons and Listboxes
	if (child)
	{
		csString type = child->getType ().c_str ();
		size_t last_slash = type.FindLast ('/');
		type = type.Slice (last_slash + 1);
		if (type == "Button")
		{
			return Action (child->getName ());
		}
	} // end if

	return false;
} // end OnClicked ()

/// Called when a key is pressed in the window.
bool preGUIWindow::OnKeyPress (const CEGUI::EventArgs& args)
{
	CEGUI::Window* window =
		static_cast <const CEGUI::WindowEventArgs&> (args).window->getActiveChild ();
	CEGUI::Key::Scan scancode =
		static_cast <const CEGUI::KeyEventArgs&> (args).scancode;

	if (scancode == CEGUI::Key::Return || scancode == CEGUI::Key::NumpadEnter)
	{
		if (window->isChildRecursive (2695))
		{
			CEGUI::Window* child = window->getChildRecursive (2695);

			if (child != NULL)
			{
				return Action (child->getName ());
			} // end if
		} // end if
	}
	else if (scancode == CEGUI::Key::Escape)
	{
		if (window->isChildRecursive (2696))
		{
			CEGUI::Window* child = window->getChildRecursive (2696);

			if (child != NULL)
			{
				return Action (child->getName ());
			} // end if
		} // end if
	} // end if

	return false;
} // end OnKeyPress ()

/// Perform the action associated with the given control.
bool preGUIWindow::Action (const CEGUI::String windowName)
{
	return false;
}

/// @}

