////////////////////////////////////////////////////////////////////////////////
/**
 * Copyright (C) 2004-2006 G33X Nexus Entertainment (http://gne.g33xnexus.com/)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////
/**
 * @file GUIEventInjector.cpp
 *
 * @author Seth Yastrov <syastrov@gmail.com>
 *
 * @brief Definition of GUIEventInjector, CEGUI event injector
 */
////////////////////////////////////////////////////////////////////////////////

// Precursors Includes
#include "global.h"

// CEGUI Includes
#include "CEGUI.h"
#include "CEGUIInputEvent.h"

// Precursors Includes
#include "iwindowmanager/GUIEventInjector.h"

// CS Includes
#include "cssysdef.h"
#include "ivaria/reporter.h"
#include "ivaria/icegui.h"
#include "csutil/csevcord.h"
#include "csutil/eventnames.h"

namespace Precursors
{
	////////////////////////////////////////////////////////////////////////////
	// Constructor and Destructor.

	GUIEventInjector::GUIEventInjector (iObjectRegistry* object_reg)
		: scfImplementationType (this)
	{
		GUIEventInjector::object_reg = object_reg;

		cegui = csQueryRegistry<iCEGUI> (object_reg);
		if (!cegui)
		{
			csReport (object_reg, CS_REPORTER_SEVERITY_ERROR, "precursors.addon.contentloader", "Can't find the CEGUI plugin!");
		} // end if

		SetupEvents ();

		GUIEventInjector::name_reg = csEventNameRegistry::GetRegistry (object_reg);

		// Create the event outlet
		csRef<iEventQueue> queue = csQueryRegistry<iEventQueue> (object_reg);
		if (!queue)
		{
			csReport (object_reg, CS_REPORTER_SEVERITY_ERROR, "precursors.addon.contentloader", "Can't find the event queue!");
		}
		else
		{
			eventOutlet = queue->CreateEventOutlet (this);
		}
	} // end GUIEventInjector ()

	GUIEventInjector::GUIEventInjector (iObjectRegistry* object_reg, CEGUI::Window* window)
		: scfImplementationType (this)
	{
		GUIEventInjector::object_reg = object_reg;

		cegui = csQueryRegistry<iCEGUI> (object_reg);
		if (!cegui)
		{
			csReport (object_reg, CS_REPORTER_SEVERITY_ERROR, "precursors.addon.contentloader", "Can't find the CEGUI plugin!");
		} // end if

		SetupEvents (window);

		GUIEventInjector::name_reg = csEventNameRegistry::GetRegistry (object_reg);

		// Create the event outlet
		csRef<iEventQueue> queue = csQueryRegistry<iEventQueue> (object_reg);
		if (!queue)
		{
			csReport (object_reg, CS_REPORTER_SEVERITY_ERROR, "precursors.addon.contentloader", "Can't find the event queue!");
		}
		else
		{
			eventOutlet = queue->CreateEventOutlet (this);
		}
	} // end GUIEventInjector ()

	GUIEventInjector::~GUIEventInjector ()
	{
	} // end ~GUIEventInjector ()

	void GUIEventInjector::SetupEvents ()
	{
		SetupEvents (cegui->GetSystemPtr ()->getGUISheet ());
	} // end SetupEvents ()

	void GUIEventInjector::SetupEvents (CEGUI::Window* window)
	{
		// If we're already connected, disconnect first.
		if (connection.isValid ())
		{
			connection->disconnect ();
		} // end if

		// Set the single click timeout to infinity
		cegui->GetSystemPtr ()->setSingleClickTimeout (100000000.0);

		// Subscribe to all mouse click events in the root window
		connection = window->subscribeEvent (CEGUI::Window::EventMouseClick,
			CEGUI::Event::Subscriber (&GUIEventInjector::OnClicked, this));

		// Subscribe to all mouse click events in the root window
		connection = window->subscribeEvent (CEGUI::Window::EventKeyDown,
			CEGUI::Event::Subscriber (&GUIEventInjector::OnKeyPress, this));
	} // end SetupEvents ()

	bool GUIEventInjector::OnKeyPress (const CEGUI::EventArgs& args)
	{
		CEGUI::Window* window =
			static_cast <const CEGUI::WindowEventArgs&> (args).window->getActiveChild ();
		CEGUI::Key::Scan scancode =
			static_cast <const CEGUI::KeyEventArgs&> (args).scancode;

		if (scancode == CEGUI::Key::Return || scancode == CEGUI::Key::NumpadEnter)
		{
			if (window->isChildRecursive (2695))
			{
				CEGUI::Window* child = window->getChildRecursive (2695);

				if (child != NULL)
				{
					SendMenuEvent (child->getName ().c_str ());

					return true;
				} // end if
			} // end if
		}
		else if (scancode == CEGUI::Key::Escape)
		{
			if (window->isChildRecursive (2696))
			{
				CEGUI::Window* child = window->getChildRecursive (2696);

				if (child != NULL)
				{
					SendMenuEvent (child->getName ().c_str ());

					return true;
				} // end if
			} // end if
		} // end if

		return false;
	} // end OnKeyPress ()

	bool GUIEventInjector::OnClicked (const CEGUI::EventArgs& args)
	{
		CEGUI::Window* window =
			static_cast <const CEGUI::WindowEventArgs&> (args).window;
		CEGUI::Point position =
			static_cast <const CEGUI::MouseEventArgs&> (args).position;

		CEGUI::Window* child = window->getChildAtPosition (position);

		// Only post action events for Buttons
		if (!child || child->getType ().substr (child->getType ().length ()-7, 7) != "/Button")
		{
			return false;
		} // end if

		SendMenuEvent (child->getName ().c_str ());

		return true;
	} // end OnClicked ()

	void GUIEventInjector::SendMenuEvent (const char* action)
	{
		// Create menu action event
		csRef<iEvent> event = eventOutlet->CreateEvent ();
		event->Name = preevMenuAction (object_reg);
		event->Broadcast = true;
		event->Time = csGetTicks ();
		event->Add ("action", action);

		csRef<iEventQueue> queue = csQueryRegistry<iEventQueue> (object_reg);

		// Broadcast it (using the queue instead of the event outlet because the event outlet is a big floppy donkey dick.)
		queue->Post (event);
	} // end SendMenuEvent ()
} // end namespace Precursors

