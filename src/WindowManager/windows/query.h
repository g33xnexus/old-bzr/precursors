////////////////////////////////////////////////////////////////////////////////
/**
 * Copyright (C) 2004-2006 G33X Nexus Entertainment (http://gne.g33xnexus.com/)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////
/**
 * @file preWindowQuery.h
 * @author David Bronke <David.Bronke@g33xnexus.com>
 *
 * @brief Declaration of the Precursors GUI window base class.
 */
////////////////////////////////////////////////////////////////////////////////


#ifndef __preWindowQuery_h__
#define __preWindowQuery_h__

#include "iwindowmanager/preGUIWindow.h"

// Forward declarations
struct iObjectRegistry;


/**
 * The content loader interface.
 */
struct preWindowQuery : public scfImplementationExt0<preWindowQuery, preGUIWindow>
{
public:
	////////////////////////////////////////////////////////////////////////
	/// Constructors and destructors
	/// @{

	/// Constructor.
	preWindowQuery (iObjectRegistry* object_reg);

	/// Destructor.
	virtual ~preWindowQuery ();

	/// @}


	////////////////////////////////////////////////////////////////////////
	/// Window Management Functions
	/// @{

	/**
	 * Send this window a message.
	 * @return true on success, false otherwise.
	 */
	virtual bool SendMessage (const char* message, csHash<csVariant, csString> parameters);

	/// @}


	////////////////////////////////////////////////////////////////////////
	/// Event Handlers
	/// @{

	/// Perform the action associated with the given control.
	virtual bool Action (const CEGUI::String windowName);

	/// @}
private:
	csString owner;
	csString selectedItem;
}; // end struct preWindowQuery

#endif // __preWindowQuery_h__

