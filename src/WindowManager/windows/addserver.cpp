////////////////////////////////////////////////////////////////////////////////
/**
 * Copyright (C) 2004-2006 G33X Nexus Entertainment (http://gne.g33xnexus.com/)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////
/**
 * @file preWindowAddServer.cpp
 *
 * @author David H. Bronke <David.Bronke@g33xnexus.com>
 *
 * @brief Definition of preWindowAddServer, the Precursors GUI window base class.
 */
////////////////////////////////////////////////////////////////////////////////

// Precursors Includes
#include "global.h"

// CEGUI Includes
#include "CEGUI.h"
#include "CEGUIInputEvent.h"

// Precursors Includes
#include "addserver.h"

// CS Includes
#include "cssysdef.h"
#include "ivaria/reporter.h"
#include "ivaria/icegui.h"
#include "iutil/eventq.h"
#include "iutil/pluginconfig.h"
#include "csutil/csevcord.h"
#include "csutil/eventnames.h"


////////////////////////////////////////////////////////////////////////
/// Constructors and destructors
/// @{

/// Constructor.
preWindowAddServer::preWindowAddServer (iObjectRegistry* object_reg)
	: scfImplementationType (this, object_reg)
{
	window = windowManager->LoadCEGUILayout ("menu/addserver.layout");

	// Subscribe to all keypress events in this window
	keyboardCon = window->subscribeEvent (CEGUI::Window::EventKeyDown,
		CEGUI::Event::Subscriber (&preGUIWindow::OnKeyPress, (preGUIWindow*) this));

	// Subscribe to all mouse click events in this window
	mouseCon = window->subscribeEvent (CEGUI::Window::EventMouseClick,
		CEGUI::Event::Subscriber (&preGUIWindow::OnClicked, (preGUIWindow*) this));
} // end preWindowAddServer ()

/// Destructor.
preWindowAddServer::~preWindowAddServer ()
{
} // end ~preWindowAddServer ()

/// @}


////////////////////////////////////////////////////////////////////////
/// Window Management Methods
/// @{

/**
 * Send this window a message.
 * @return true on success, false otherwise.
 */
bool preWindowAddServer::SendMessage (const char* message, csHash<csVariant, csString> parameters)
{
	csString msg = message;
	if (msg == "SetOwner")
	{
		csVariant* param = parameters.GetElementPointer ("owner");
		if (param == NULL)
		{
			csReport (object_reg, CS_REPORTER_SEVERITY_WARNING, "precursors.addon.window.addserver", "Message 'SetOwner' missing parameter 'owner'!");
			return false;
		}
		if (param->GetType () != CSVAR_STRING)
		{
			csReport (object_reg, CS_REPORTER_SEVERITY_WARNING, "precursors.addon.window.addserver", "Wrong type for parameter 'owner' to SetOwner! (should be string)");
			return false;
		}
		owner = param->GetString ();
		return true;
	}
	return false;
} // end SendMessage ()

/// @}


////////////////////////////////////////////////////////////////////////
/// Event Handlers
/// @{

/// Perform the action associated with the given control.
bool preWindowAddServer::Action (const CEGUI::String windowName)
{
	if (windowName == "Precursors/AddServerWindow/Cancel")
	{
		//TODO: Destroy the window?
		Hide ();
	}
	else if (windowName == "Precursors/AddServerWindow/Add")
	{
		CEGUI::String nameString = ceguiWinMgr->getWindow ("Precursors/AddServerWindow/Name")->getText ();
		CEGUI::String addressString = ceguiWinMgr->getWindow ("Precursors/AddServerWindow/Address")->getText ();
		CEGUI::String portString = ceguiWinMgr->getWindow ("Precursors/AddServerWindow/Port")->getText ();
		unsigned int port;
		sscanf (portString.c_str (), "%u", &port);

		csReport (object_reg, CS_REPORTER_SEVERITY_NOTIFY, "precursors.addon.window.addserver", "Adding server %s:%d.", addressString.c_str (), port);

		if (owner.Length () > 0)
		{
			csHash<csVariant, csString> params;
			csVariant csvName, csvAddress, csvPort;

			csvName.SetString (nameString.c_str ());
			params.Put ("name", csvName);
			csvAddress.SetString (addressString.c_str ());
			params.Put ("address", csvAddress);
			csvPort.SetLong (port);
			params.Put ("port", csvPort);

			csRef<iPreGUIWindow> ownerWindow = windowManager->FindWindow (owner);
			if (ownerWindow.IsValid ())
			{
				ownerWindow->SendMessage ("AddServer", params);
			}
			else
			{
				csReport (object_reg, CS_REPORTER_SEVERITY_ERROR, "precursors.addon.window.addserver", "Couldn't find owner window %s!", owner.GetData ());
			} // end if
		}
		else
		{
			csReport (object_reg, CS_REPORTER_SEVERITY_ERROR, "precursors.addon.window.addserver", "No owner window set!");
		} // end if

		//TODO: Destroy the window?
		Hide ();
	}
	else
	{
		return false;
	} // end if
	return true;
}

/// @}

