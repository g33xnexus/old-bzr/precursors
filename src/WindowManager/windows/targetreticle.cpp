////////////////////////////////////////////////////////////////////////////////
/**
 * Copyright (C) 2004-2006 G33X Nexus Entertainment (http://gne.g33xnexus.com/)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////
/**
 * @file preWindowTargetingReticle.cpp
 *
 * @author David H. Bronke <David.Bronke@g33xnexus.com>
 *
 * @brief Definition of preWindowTargetingReticle, the Precursors GUI window base class.
 */
////////////////////////////////////////////////////////////////////////////////

// Precursors Includes
#include "global.h"

// CEGUI Includes
#include "CEGUI.h"
#include "CEGUIInputEvent.h"

// Precursors Includes
#include "targetreticle.h"

// CS Includes
#include "cssysdef.h"
#include "ivaria/reporter.h"
#include "ivaria/icegui.h"
#include "ivaria/view.h"
#include "ivaria/view.h"
#include "igeom/clip2d.h"
#include "iutil/eventq.h"
#include "iutil/pluginconfig.h"
#include "iengine/camera.h"
#include "iengine/mesh.h"
#include "iengine/movable.h"
#include "ivideo/graph3d.h"
#include "csgeom/transfrm.h"
#include "csgeom/math2d.h"
#include "csutil/csevcord.h"
#include "csutil/eventnames.h"

// CEL Includes
#include "physicallayer/entity.h"
#include "physicallayer/propclas.h"
#include "propclass/mesh.h"
#include "propclass/camera.h"

#define CS_REPORT(severity,message) csReport (object_reg, CS_REPORTER_SEVERITY_##severity, "precursors.addon.window.targetingreticle", message);


////////////////////////////////////////////////////////////////////////
/// Constructors and destructors
/// @{

/// Constructor.
preWindowTargetingReticle::preWindowTargetingReticle (iObjectRegistry* object_reg)
	: scfImplementationType (this, object_reg)
{
	window = windowManager->LoadCEGUIWindow ("SleekSpace/TargetingReticle");
	window->setProperty ("BackgroundEnabled", "false");
	window->setProperty ("FrameEnabled", "true");
	window->setProperty ("FrameColour", "BBFF0000");
	window->setProperty ("TextColour", "BB888888");
	window->setVisible (true);
	window->setEnabled (true);
	window->setMinSize (CEGUI::UVector2 (CEGUI::UDim (0, 65), CEGUI::UDim (0, 65)));

	// Subscribe to all keypress events in this window
	keyboardCon = window->subscribeEvent (CEGUI::Window::EventKeyDown,
		CEGUI::Event::Subscriber (&preGUIWindow::OnKeyPress, (preGUIWindow*) this));

	// Subscribe to all mouse click events in this window
	mouseCon = window->subscribeEvent (CEGUI::Window::EventMouseClick,
		CEGUI::Event::Subscriber (&preGUIWindow::OnClicked, (preGUIWindow*) this));

	g3d = csQueryRegistry<iGraphics3D> (object_reg);
	if (!g3d.IsValid ())
	{
		CS_REPORT (WARNING, "Can't find the 3D graphics plugin!")
	} // end if

	pl = csQueryRegistry<iCelPlLayer> (object_reg);
	if (!pl.IsValid ())
	{
		CS_REPORT (WARNING, "Can't find the CEL physical layer!")
	} // end if

	pl->CallbackEveryFrame ((iCelTimerListener*) this, CEL_EVENT_PRE);
} // end preWindowTargetingReticle ()

/// Destructor.
preWindowTargetingReticle::~preWindowTargetingReticle ()
{
} // end ~preWindowTargetingReticle ()

/// @}


////////////////////////////////////////////////////////////////////////
/// Window Management Methods
/// @{

/**
 * Send this window a message.
 * @return true on success, false otherwise.
 */
bool preWindowTargetingReticle::SendMessage (const char* message, csHash<csVariant, csString> parameters)
{
	csString msg = message;
	if (msg == "SetTarget")
	{
		csVariant* param = parameters.GetElementPointer ("target");
		if (param == NULL)
		{
			CS_REPORT (WARNING, "Message 'SetTarget' missing parameter 'target'!")
			return false;
		} // end if

		if (param->GetType () != CSVAR_LONG)
		{
			CS_REPORT (WARNING, "Wrong type for parameter 'target' to SetTarget! (should be string)")
			return false;
		} // end if

		csRef<iCelEntity> targetent = pl->GetEntity ((uint) param->GetLong ());
		if (!targetent.IsValid ())
		{
			CS_REPORT (WARNING, "Can't find the target entity!")
			return false;
		} // end if

		target = CEL_QUERY_PROPCLASS_ENT (targetent, iPcMesh);
		if (!target.IsValid ())
		{
			CS_REPORT (WARNING, "Can't find the target entity's mesh!")
			return false;
		} // end if
	}
	else if (msg == "SetCamera")
	{
		csVariant* param = parameters.GetElementPointer ("camera");
		if (param == NULL)
		{
			CS_REPORT (WARNING, "Message 'SetCamera' missing parameter 'camera'!")
			return false;
		} // end if

		if (param->GetType () != CSVAR_LONG)
		{
			CS_REPORT (WARNING, "Wrong type for parameter 'camera' to SetCamera! (should be string)")
			return false;
		} // end if

		csRef<iCelEntity> cameraent = pl->GetEntity ((uint) param->GetLong ());
		if (!cameraent.IsValid ())
		{
			CS_REPORT (WARNING, "Can't find the camera entity!")
			return false;
		} // end if

		csRef<iPcCamera> camerapc = CEL_QUERY_PROPCLASS_ENT (cameraent, iPcCamera);
		if (!camerapc.IsValid ())
		{
			CS_REPORT (WARNING, "Can't find the camera property class!")
			return false;
		} // end if

		camera = camerapc->GetCamera ();
		if (!camera.IsValid ())
		{
			CS_REPORT (WARNING, "Can't find the camera!")
			return false;
		} // end if

		clipper = camerapc->GetView ()->GetClipper ();
		if (!clipper.IsValid ())
		{
			CS_REPORT (WARNING, "Can't find the 2D clipper!")
			return false;
		} // end if
	}
	else
	{
		return false;
	} // end if

	return true;
} // end SendMessage ()

/// @}


////////////////////////////////////////////////////////////////////////
/// Event Handlers
/// @{

/// Perform the action associated with the given control.
bool preWindowTargetingReticle::Action (const CEGUI::String windowName)
{
	if (windowName == window->getName ())
	{
		//TODO: Select given target.
		CS_REPORT (NOTIFY, "Ooh! Clicky!")
		return true;
	} // end if

	return false;
} // end Action ()

/// @}


////////////////////////////////////////////////////////////////////////////
/// iCelTimerListener implementation
/// @{

/// Process all incoming packets each frame.
void preWindowTargetingReticle::TickEveryFrame ()
{
	if (target.IsValid ())
	{
		csScreenBoxResult result = target->GetMesh ()->GetScreenBoundingBox (camera);
		csVector2 bboxCenter = result.sbox.GetCenter ();
		int screenHeight = g3d->GetHeight ();
		int screenWidth = g3d->GetWidth ();
		if (result.distance > 0 && clipper->IsInside (bboxCenter))
		{
			// Draw targeting reticle
			float width = result.sbox.MaxX () - result.sbox.MinX () + 10;
			if (width < 70)
			{
				window->setXPosition (CEGUI::UDim (0, result.sbox.MinX () - 5 - ((70 - width) / 2)));
				window->setWidth (CEGUI::UDim (0, 70));
			}
			else
			{
				window->setXPosition (CEGUI::UDim (0, result.sbox.MinX () - 5));
				window->setWidth (CEGUI::UDim (0, width));
			} // end if

			float height = result.sbox.MaxY () - result.sbox.MinY () + 10;
			if (height < 70)
			{
				window->setYPosition (CEGUI::UDim (0, screenHeight - result.sbox.MaxY () - 5 - ((70 - height) / 2)));
				window->setHeight (CEGUI::UDim (0, 70));
			}
			else
			{
				window->setYPosition (CEGUI::UDim (0, screenHeight - result.sbox.MaxY () - 5));
				window->setHeight (CEGUI::UDim (0, height));
			} // end if

			csString status;
			status.Format ("%.3f km", result.distance / 1000);
			window->setText (status.GetData ());
		}
		else
		{
			csVector2 screenSize = csVector2 (screenWidth, screenHeight);
			csVector2 direction = bboxCenter - (screenSize / 2);
			csVector2 position;

			if (direction.x == 0)
			{
				// We'll be testing against the top or the bottom.
				if (bboxCenter.y > (screenHeight / 2))
				{
					// We'll test against the bottom side.
					csIntersect2::LineLine (csSegment2 (screenSize / 2, bboxCenter), csSegment2 (csVector2 (0, screenHeight), screenSize), position);
				}
				else
				{
					// We'll test against the top side.
					csIntersect2::LineLine (csSegment2 (screenSize / 2, bboxCenter), csSegment2 (csVector2 (), csVector2 (screenWidth, 0)), position);
				} // end if
			}
			else
			{
				float screenSlope = screenSize.y / screenSize.x;
				float directionSlope = direction.y / direction.x;

				if (fabs (directionSlope) >= screenSlope)
				{
					// We'll be testing against the top or the bottom.
					if (bboxCenter.y > (screenHeight / 2))
					{
						// We'll test against the bottom side.
						csIntersect2::LineLine (csSegment2 (screenSize / 2, bboxCenter), csSegment2 (csVector2 (0, screenHeight), screenSize), position);
					}
					else
					{
						// We'll test against the top side.
						csIntersect2::LineLine (csSegment2 (screenSize / 2, bboxCenter), csSegment2 (csVector2 (), csVector2 (screenWidth, 0)), position);
					} // end if
				}
				else
				{
					// We'll be testing against one of the sides.
					if (bboxCenter.x > (screenWidth / 2))
					{
						// We'll test against the right side.
						csIntersect2::LineLine (csSegment2 (screenSize / 2, bboxCenter), csSegment2 (csVector2 (screenWidth, 0), screenSize), position);
					}
					else
					{
						// We'll test against the left side.
						csIntersect2::LineLine (csSegment2 (screenSize / 2, bboxCenter), csSegment2 (csVector2 (), csVector2 (0, screenHeight)), position);
					} // end if
				} // end if
			} // end if

			int arrowDimension = 70;
			window->setWidth (CEGUI::UDim (0, arrowDimension * 2));
			window->setHeight (CEGUI::UDim (0, arrowDimension * 2));
			window->setXPosition (CEGUI::UDim (0, position.x - arrowDimension));
			window->setYPosition (CEGUI::UDim (0, screenHeight - position.y - arrowDimension));
		} // end if
	} // end if
} // end TickEveryFrame ()

/// @}

