////////////////////////////////////////////////////////////////////////////////
/**
 * Copyright (C) 2004-2006 G33X Nexus Entertainment (http://gne.g33xnexus.com/)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////
/**
 * @file preWindowMenu.cpp
 *
 * @author David H. Bronke <David.Bronke@g33xnexus.com>
 *
 * @brief Definition of preWindowMenu, the Precursors GUI window base class.
 */
////////////////////////////////////////////////////////////////////////////////

// Precursors Includes
#include "global.h"

// CEGUI Includes
#include "CEGUI.h"
#include "CEGUIInputEvent.h"

// Precursors Includes
#include "menu.h"

// CS Includes
#include "cssysdef.h"
#include "ivaria/reporter.h"
#include "ivaria/icegui.h"
#include "iutil/eventq.h"
#include "iutil/pluginconfig.h"
#include "csutil/csevcord.h"
#include "csutil/eventnames.h"


////////////////////////////////////////////////////////////////////////
/// Constructors and destructors
/// @{

/// Constructor.
preWindowMenu::preWindowMenu (iObjectRegistry* object_reg)
	: scfImplementationType (this, object_reg)
{
	window = windowManager->LoadCEGUILayout ("menu/menu.layout");

	// Subscribe to all keypress events in this window
	keyboardCon = window->subscribeEvent (CEGUI::Window::EventKeyDown,
		CEGUI::Event::Subscriber (&preGUIWindow::OnKeyPress, (preGUIWindow*) this));

	// Subscribe to all mouse click events in this window
	mouseCon = window->subscribeEvent (CEGUI::Window::EventMouseClick,
		CEGUI::Event::Subscriber (&preGUIWindow::OnClicked, (preGUIWindow*) this));
} // end preWindowMenu ()

/// Destructor.
preWindowMenu::~preWindowMenu ()
{
} // end ~preWindowMenu ()

/// @}


////////////////////////////////////////////////////////////////////////
/// Window Management Methods
/// @{

/**
 * Send this window a message.
 * @return true on success, false otherwise.
 */
bool preWindowMenu::SendMessage (const char* message, csArray<csVariant> parameters)
{
	return false;
} // end SendMessage ()

/// @}


////////////////////////////////////////////////////////////////////////
/// Event Handlers
/// @{

/// Perform the action associated with the given control.
bool preWindowMenu::Action (const CEGUI::String windowName)
{
	if (windowName == "")
	{
	}
	else if (windowName == "Precursors/MenuWindow/Exit")
	{
		csReport (object_reg, CS_REPORTER_SEVERITY_NOTIFY, "precursors.addon.windowmanager.login", "Screw you guys. I'm goin home.");
		csRef<iEventQueue> queue = csQueryRegistry<iEventQueue> (object_reg);
		if (queue.IsValid ())
		{
			queue->GetEventOutlet ()->Broadcast (csevQuit (object_reg));
		}
		else
		{
			csReport (object_reg, CS_REPORTER_SEVERITY_ERROR, "precursors.addon.windowmanager.login", "Can't find the event queue!");
		}
	}
	else
	{
		return false;
	}
	return true;
}

/// @}

