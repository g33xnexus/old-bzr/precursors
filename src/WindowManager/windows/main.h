////////////////////////////////////////////////////////////////////////////////
/**
 * Copyright (C) 2004-2006 G33X Nexus Entertainment (http://gne.g33xnexus.com/)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////
/**
 * @file preWindowMain.h
 * @author David Bronke <David.Bronke@g33xnexus.com>
 *
 * @brief Declaration of the Precursors GUI window base class.
 */
////////////////////////////////////////////////////////////////////////////////


#ifndef __preWindowMain_h__
#define __preWindowMain_h__

#include "iwindowmanager/preGUIWindow.h"

// Forward declarations
struct iObjectRegistry;
struct iConfigManager;


/**
 * The content loader interface.
 */
struct preWindowMain : public scfImplementationExt0<preWindowMain, preGUIWindow>
{
public:
	////////////////////////////////////////////////////////////////////////
	/// Constructors and destructors
	/// @{

	/// Constructor.
	preWindowMain (iObjectRegistry* object_reg);

	/// Destructor.
	virtual ~preWindowMain ();

	/// @}


	////////////////////////////////////////////////////////////////////////
	/// Window Management Functions
	/// @{

	/**
	 * Send this window a message.
	 * @return true on success, false otherwise.
	 */
	virtual bool SendMessage (const char* message, csHash<csVariant, csString> parameters);

	/// @}


	////////////////////////////////////////////////////////////////////////
	/// Event Handlers
	/// @{

	/// Perform the action associated with the given control.
	virtual bool Action (const CEGUI::String windowName);

	/// @}


	////////////////////////////////////////////////////////////////////////
	/// Helper Methods
	/// @{

	/// Add available choices to the forms in this window.
	void PopulateForms ();

	/// @}

private:
	void AddServer (const char* name, const char* address, uint port = 0, bool addToConfigFile = true);

	void ModifyServer (const char* oldName, const char* newName, const char* address, uint port = 0, bool addToConfigFile = true);

	void RemoveServer (const char* name);

	bool OnServerSelected (const CEGUI::EventArgs& args);

	struct ServerInfo
	{
		ServerInfo (const char* add, uint port)
		{
			address = add;
			ServerInfo::port = port;
		}

		ServerInfo (const char* add)
		{
			address = add;
			size_t pos = address.FindLast (':');
			csString portString = address.Slice (pos + 1);
			sscanf (portString.GetData (), "%u", &port);
			address.Truncate (pos);
		}

		bool operator==(const struct ServerInfo& right) const
		{
			if ((address == right.address) && (port == right.port))
				return true;
			else
				return false;
		}
		csString address;
		uint port;
	};

	csHash<ServerInfo, csString> serverData;

	CEGUI::ItemListbox* serverList;

	CEGUI::Window* status;

	csWeakRef<iConfigManager> applicationConfig;

	csArray<CEGUI::Event::Connection> serverListConnections;

	csString selectedServer;
}; // end struct preWindowMain

#endif // __preWindowMain_h__

