////////////////////////////////////////////////////////////////////////////////
/**
 * Copyright (C) 2004-2006 G33X Nexus Entertainment (http://gne.g33xnexus.com/)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////
/**
 * @file preWindowTargetingReticle.h
 * @author David Bronke <David.Bronke@g33xnexus.com>
 *
 * @brief Declaration of the Precursors GUI window base class.
 */
////////////////////////////////////////////////////////////////////////////////


#ifndef __preWindowTargetingReticle_h__
#define __preWindowTargetingReticle_h__

#include "iwindowmanager/preGUIWindow.h"
#include "physicallayer/pl.h"

// Forward declarations
struct iObjectRegistry;
struct iPcMesh;
struct iCamera;
struct iClipper2D;
struct iGraphics3D;


/**
 * The content loader interface.
 */
struct preWindowTargetingReticle : public scfImplementationExt1<preWindowTargetingReticle, preGUIWindow, iCelTimerListener>
{
public:
	////////////////////////////////////////////////////////////////////////
	/// Constructors and destructors
	/// @{

	/// Constructor.
	preWindowTargetingReticle (iObjectRegistry* object_reg);

	/// Destructor.
	virtual ~preWindowTargetingReticle ();

	/// @}


	////////////////////////////////////////////////////////////////////////
	/// Window Management Methods
	/// @{

	/**
	 * Send this window a message.
	 * @return true on success, false otherwise.
	 */
	virtual bool SendMessage (const char* message, csHash<csVariant, csString> parameters);

	/// @}


	////////////////////////////////////////////////////////////////////////
	/// Event Handlers
	/// @{

	/// Perform the action associated with the given control.
	virtual bool Action (const CEGUI::String windowName);

	/// @}


	////////////////////////////////////////////////////////////////////////////
	/// iCelTimerListener implementation
	/// @{

	/// Process all incoming packets each frame.
	virtual void TickEveryFrame ();

	/// Ignored.
	virtual void TickOnce () {}

	/// @}
private:
	csWeakRef<iPcMesh> target;

	csWeakRef<iCamera> camera;

	csWeakRef<iClipper2D> clipper;

	csWeakRef<iGraphics3D> g3d;

	csWeakRef<iCelPlLayer> pl;
}; // end struct preWindowTargetingReticle

#endif // __preWindowTargetingReticle_h__

