////////////////////////////////////////////////////////////////////////////////
/**
 * Copyright (C) 2004-2006 G33X Nexus Entertainment (http://gne.g33xnexus.com/)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////
/**
 * @file preWindowMain.cpp
 *
 * @author David H. Bronke <David.Bronke@g33xnexus.com>
 *
 * @brief Definition of preWindowMain, the Precursors GUI window base class.
 */
////////////////////////////////////////////////////////////////////////////////

// Precursors Includes
#include "global.h"

// CEGUI Includes
#include "CEGUI.h"
#include "CEGUIInputEvent.h"
#include "elements/CEGUIListbox.h"
#include "elements/CEGUIListboxItem.h"

// Precursors Includes
#include "main.h"
//#include "iPreContentManager.h"

// CS Includes
#include "cssysdef.h"
#include "ivaria/reporter.h"
#include "ivaria/icegui.h"
#include "iutil/eventq.h"
#include "iutil/pluginconfig.h"
#include "iutil/cfgmgr.h"
#include "iutil/vfs.h"
#include "iutil/stringarray.h"
#include "csutil/csevcord.h"
#include "csutil/csstring.h"
#include "csutil/eventnames.h"
#include "ivideo/graph3d.h"

#define CS_REPORT(severity,message) csReport (object_reg, CS_REPORTER_SEVERITY_##severity, "precursors.addon.window.main", message);


////////////////////////////////////////////////////////////////////////
/// Constructors and destructors
/// @{

/// Constructor.
preWindowMain::preWindowMain (iObjectRegistry* object_reg)
	: scfImplementationType (this, object_reg)
{
	window = windowManager->LoadCEGUILayout ("menu/main.layout");

	// 
	// windowManager->EnableMouse();Subscribe to all keypress events in this window
	keyboardCon = window->subscribeEvent (CEGUI::Window::EventKeyDown,
		CEGUI::Event::Subscriber (&preGUIWindow::OnKeyPress, (preGUIWindow*) this));

	// Subscribe to all mouse click events in this window
	mouseCon = window->subscribeEvent (CEGUI::Window::EventMouseClick,
		CEGUI::Event::Subscriber (&preGUIWindow::OnClicked, (preGUIWindow*) this));

	// Get a pointer to the configuration manager.
	applicationConfig = csQueryRegistry<iConfigManager> (object_reg);

	// Get pointers to important controls in this window.
	serverList = (CEGUI::ItemListbox*) ceguiWinMgr->getWindow ("Precursors/MainWindow/Online/ServerList");
	status = ceguiWinMgr->getWindow ("Precursors/MainWindow/Online/StatusLabel");

	PopulateForms ();
} // end preWindowMain ()

/// Destructor.
preWindowMain::~preWindowMain ()
{
} // end ~preWindowMain ()

/// @}


////////////////////////////////////////////////////////////////////////
/// Window Management Functions
/// @{

/**
 * Send this window a message.
 * @return true on success, false otherwise.
 */
bool preWindowMain::SendMessage (const char* message, csHash<csVariant, csString> parameters)
{
	csString msg = message;
	if (msg == "AddServer")
	{
		csVariant* paramName = parameters.GetElementPointer ("name");
		csVariant* paramAddress = parameters.GetElementPointer ("address");
		csVariant* paramPort = parameters.GetElementPointer ("port");

		if (paramName == NULL)
		{
			CS_REPORT (WARNING, "Message 'AddServer' missing parameter 'name'!")
			return false;
		} // end if

		if (paramName->GetType () != CSVAR_STRING)
		{
			CS_REPORT (WARNING, "Wrong type for parameter 'name' to AddServer! (should be string)")
			return false;
		} // end if

		if (paramAddress == NULL)
		{
			CS_REPORT (WARNING, "Message 'AddServer' missing parameter 'address'!")
			return false;
		} // end if

		if (paramAddress->GetType () != CSVAR_STRING)
		{
			CS_REPORT (WARNING, "Wrong type for parameter 'address' to AddServer! (should be string)")
			return false;
		} // end if

		if (paramPort == NULL)
		{
			CS_REPORT (WARNING, "Message 'AddServer' missing parameter 'port'!")
			return false;
		} // end if

		if (paramPort->GetType () != CSVAR_LONG)
		{
			CS_REPORT (WARNING, "Wrong type for parameter 'port' to AddServer! (should be long)")
			return false;
		} // end if

		// Add bookmark to the server list.
		AddServer (paramName->GetString (), paramAddress->GetString (), paramPort->GetLong ());
	}
	else if (msg == "ModifyServer")
	{
		csVariant* paramOldName = parameters.GetElementPointer ("oldname");
		csVariant* paramName = parameters.GetElementPointer ("name");
		csVariant* paramAddress = parameters.GetElementPointer ("address");
		csVariant* paramPort = parameters.GetElementPointer ("port");
		
		if (paramOldName == NULL)
		{
			CS_REPORT (WARNING, "Message 'ModifyServer' missing parameter 'oldname'!")
			return false;
		} // end if

		if (paramOldName->GetType () != CSVAR_STRING)
		{
			CS_REPORT (WARNING, "Wrong type for parameter 'oldname' to ModifyServer! (should be string)")
			return false;
		} // end if

		if (paramName == NULL)
		{
			CS_REPORT (WARNING, "Message 'ModifyServer' missing parameter 'name'!")
			return false;
		} // end if

		if (paramName->GetType () != CSVAR_STRING)
		{
			CS_REPORT (WARNING, "Wrong type for parameter 'name' to ModifyServer! (should be string)")
			return false;
		} // end if

		if (paramAddress == NULL)
		{
			CS_REPORT (WARNING, "Message 'ModifyServer' missing parameter 'address'!")
			return false;
		} // end if

		if (paramAddress->GetType () != CSVAR_STRING)
		{
			CS_REPORT (WARNING, "Wrong type for parameter 'address' to ModifyServer! (should be string)")
			return false;
		} // end if

		if (paramPort == NULL)
		{
			CS_REPORT (WARNING, "Message 'ModifyServer' missing parameter 'port'!")
			return false;
		} // end if

		if (paramPort->GetType () != CSVAR_LONG)
		{
			CS_REPORT (WARNING, "Wrong type for parameter 'port' to ModifyServer! (should be long)")
			return false;
		} // end if

		// Modify bookmark.
		ModifyServer (paramOldName->GetString (), paramName->GetString (), paramAddress->GetString (), paramPort->GetLong ());
	}
	else if (msg == "Query")
	{
		//The funny part is we only send back an answer if the answer is true!
		csVariant* paramServer = parameters.GetElementPointer ("selectedItem");

		if (paramServer == NULL)
		{
			CS_REPORT (WARNING, "Message 'Query' missing parameter 'selectedItem'!")
			return false;
		} // end if

		if (paramServer->GetType () != CSVAR_STRING)
		{
			CS_REPORT (WARNING, "Wrong type for parameter 'selectedItem' to Query! (should be string)")
			return false;
		} // end if

		RemoveServer (paramServer->GetString ());
	}
	else
	{
		return false;
	} // end if
	return true;
} // end SendMessage ()

/// @}


////////////////////////////////////////////////////////////////////////
/// Event Handlers
/// @{

/// Perform the action associated with the given control.
bool preWindowMain::Action (const CEGUI::String windowName)
{
	if (windowName == "Precursors/MainWindow/Quit")
	{
		CS_REPORT (NOTIFY, "Screw you guys. I'm goin home.")
		csRef<iEventQueue> queue = csQueryRegistry<iEventQueue> (object_reg);
		if (queue.IsValid ())
		{
			queue->GetEventOutlet ()->Broadcast (csevQuit (object_reg));
		}
		else
		{
			CS_REPORT (ERROR, "Can't find the event queue!")
			return false;
		} // end if
	}
	else if (windowName == "Precursors/MainWindow/Online")
	{
		ceguiWinMgr->getWindow ("Precursors/MainWindow/OnlineTab")->setVisible (true);
		ceguiWinMgr->getWindow ("Precursors/MainWindow/OnlineNub")->setVisible (true);
		ceguiWinMgr->getWindow ("Precursors/MainWindow/LocalTab")->setVisible (false);
		ceguiWinMgr->getWindow ("Precursors/MainWindow/LocalNub")->setVisible (false);
		ceguiWinMgr->getWindow ("Precursors/MainWindow/SettingsTab")->setVisible (false);
		ceguiWinMgr->getWindow ("Precursors/MainWindow/SettingsNub")->setVisible (false);
	}
	else if (windowName == "Precursors/MainWindow/Local")
	{
		ceguiWinMgr->getWindow ("Precursors/MainWindow/OnlineTab")->setVisible (false);
		ceguiWinMgr->getWindow ("Precursors/MainWindow/OnlineNub")->setVisible (false);
		ceguiWinMgr->getWindow ("Precursors/MainWindow/LocalTab")->setVisible (true);
		ceguiWinMgr->getWindow ("Precursors/MainWindow/LocalNub")->setVisible (true);
		ceguiWinMgr->getWindow ("Precursors/MainWindow/SettingsTab")->setVisible (false);
		ceguiWinMgr->getWindow ("Precursors/MainWindow/SettingsNub")->setVisible (false);
	}
	else if (windowName == "Precursors/MainWindow/Settings")
	{
		ceguiWinMgr->getWindow ("Precursors/MainWindow/OnlineTab")->setVisible (false);
		ceguiWinMgr->getWindow ("Precursors/MainWindow/OnlineNub")->setVisible (false);
		ceguiWinMgr->getWindow ("Precursors/MainWindow/LocalTab")->setVisible (false);
		ceguiWinMgr->getWindow ("Precursors/MainWindow/LocalNub")->setVisible (false);
		ceguiWinMgr->getWindow ("Precursors/MainWindow/SettingsTab")->setVisible (true);
		ceguiWinMgr->getWindow ("Precursors/MainWindow/SettingsNub")->setVisible (true);
	}
	else if (windowName == "Precursors/MainWindow/Settings/Audio")
	{
		ceguiWinMgr->getWindow ("Precursors/MainWindow/Settings/AudioTab")->setVisible (true);
		ceguiWinMgr->getWindow ("Precursors/MainWindow/Settings/AudioNub")->setVisible (true);
		ceguiWinMgr->getWindow ("Precursors/MainWindow/Settings/VideoTab")->setVisible (false);
		ceguiWinMgr->getWindow ("Precursors/MainWindow/Settings/VideoNub")->setVisible (false);
		ceguiWinMgr->getWindow ("Precursors/MainWindow/Settings/InputTab")->setVisible (false);
		ceguiWinMgr->getWindow ("Precursors/MainWindow/Settings/InputNub")->setVisible (false);
		ceguiWinMgr->getWindow ("Precursors/MainWindow/Settings/NetworkTab")->setVisible (false);
		ceguiWinMgr->getWindow ("Precursors/MainWindow/Settings/NetworkNub")->setVisible (false);
	}
	else if (windowName == "Precursors/MainWindow/Settings/Video")
	{
		ceguiWinMgr->getWindow ("Precursors/MainWindow/Settings/AudioTab")->setVisible (false);
		ceguiWinMgr->getWindow ("Precursors/MainWindow/Settings/AudioNub")->setVisible (false);
		ceguiWinMgr->getWindow ("Precursors/MainWindow/Settings/VideoTab")->setVisible (true);
		ceguiWinMgr->getWindow ("Precursors/MainWindow/Settings/VideoNub")->setVisible (true);
		ceguiWinMgr->getWindow ("Precursors/MainWindow/Settings/InputTab")->setVisible (false);
		ceguiWinMgr->getWindow ("Precursors/MainWindow/Settings/InputNub")->setVisible (false);
		ceguiWinMgr->getWindow ("Precursors/MainWindow/Settings/NetworkTab")->setVisible (false);
		ceguiWinMgr->getWindow ("Precursors/MainWindow/Settings/NetworkNub")->setVisible (false);
	}
	else if (windowName == "Precursors/MainWindow/Settings/Input")
	{
		ceguiWinMgr->getWindow ("Precursors/MainWindow/Settings/AudioTab")->setVisible (false);
		ceguiWinMgr->getWindow ("Precursors/MainWindow/Settings/AudioNub")->setVisible (false);
		ceguiWinMgr->getWindow ("Precursors/MainWindow/Settings/VideoTab")->setVisible (false);
		ceguiWinMgr->getWindow ("Precursors/MainWindow/Settings/VideoNub")->setVisible (false);
		ceguiWinMgr->getWindow ("Precursors/MainWindow/Settings/InputTab")->setVisible (true);
		ceguiWinMgr->getWindow ("Precursors/MainWindow/Settings/InputNub")->setVisible (true);
		ceguiWinMgr->getWindow ("Precursors/MainWindow/Settings/NetworkTab")->setVisible (false);
		ceguiWinMgr->getWindow ("Precursors/MainWindow/Settings/NetworkNub")->setVisible (false);
	}
	else if (windowName == "Precursors/MainWindow/Settings/Network")
	{
		ceguiWinMgr->getWindow ("Precursors/MainWindow/Settings/AudioTab")->setVisible (false);
		ceguiWinMgr->getWindow ("Precursors/MainWindow/Settings/AudioNub")->setVisible (false);
		ceguiWinMgr->getWindow ("Precursors/MainWindow/Settings/VideoTab")->setVisible (false);
		ceguiWinMgr->getWindow ("Precursors/MainWindow/Settings/VideoNub")->setVisible (false);
		ceguiWinMgr->getWindow ("Precursors/MainWindow/Settings/InputTab")->setVisible (false);
		ceguiWinMgr->getWindow ("Precursors/MainWindow/Settings/InputNub")->setVisible (false);
		ceguiWinMgr->getWindow ("Precursors/MainWindow/Settings/NetworkTab")->setVisible (true);
		ceguiWinMgr->getWindow ("Precursors/MainWindow/Settings/NetworkNub")->setVisible (true);
	}
	else if (windowName == "Precursors/MainWindow/Online/AddServer")
	{
		csRef<iPreGUIWindow> addServerWindow = windowManager->LoadWindow ("AddServer");
		addServerWindow->Show ();
		csHash<csVariant, csString> params;
		csVariant owner;
		owner.SetString ("Main");
		params.Put ("owner", owner);
		addServerWindow->SendMessage ("SetOwner", params);
	}
	else if (windowName == "Precursors/MainWindow/Online/EditServer")
	{
		if (selectedServer.Length () > 0)
		{
			csRef<iPreGUIWindow> modifyServerWindow = windowManager->LoadWindow ("ModifyServer");
			modifyServerWindow->Show ();
			csHash<csVariant, csString> params;
			csVariant owner;
			owner.SetString ("Main");
			params.Put ("owner", owner);

			csVariant csvSelectedServer;
			csvSelectedServer.SetString (selectedServer);

			ServerInfo* server = serverData.GetElementPointer (selectedServer);
			csString port;

			port.Format ("%u", server->port);
			params.Put ("selectedServer", csvSelectedServer);
			ceguiWinMgr->getWindow ("Precursors/ModifyServerWindow/Name")->setText ((const char*) selectedServer);
			ceguiWinMgr->getWindow ("Precursors/ModifyServerWindow/Address")->setText ((const char*) server->address);
			ceguiWinMgr->getWindow ("Precursors/ModifyServerWindow/Port")->setText ((const char*) port);

			modifyServerWindow->SendMessage ("SetOwner", params);
		}
		else //TODO: Disable Edit unless the user has a server selected.
		{
			CS_REPORT (WARNING, "Unable to Edit a bookmark without first selecting one") 
		} //end if
	}
	else if (windowName == "Precursors/MainWindow/Online/RemoveServer")
	{
		if (selectedServer.Length () > 0)
		{
			csString queryText;

			queryText.Format("Are you sure that you want to remove '%s' from the server bookmarks?", selectedServer.GetData ());

			csRef<iPreGUIWindow> QueryWindow = windowManager->LoadWindow ("Query");
			QueryWindow->Show ();

			ceguiWinMgr->getWindow ("Precursors/QueryWindow/Text")->setText ((const char*) queryText);
			ceguiWinMgr->getWindow ("Precursors/QueryWindow")->setText ("Remove Server Bookmark?");
			ceguiWinMgr->getWindow ("Precursors/QueryWindow/OK")->setText ("Yes");
			ceguiWinMgr->getWindow ("Precursors/QueryWindow/Cancel")->setText ("No");

			csHash<csVariant, csString> params;
			csVariant owner;
			owner.SetString ("Main");
			params.Put ("owner", owner);

			csVariant csvSelectedServer;
			csvSelectedServer.SetString (selectedServer);

			params.Put ("selectedItem", csvSelectedServer);
			QueryWindow->SendMessage ("SetOwner", params);
		}
		else //TODO: Disable Remove unless the user has a server selected.
		{
			CS_REPORT (WARNING, "Unable to Remove a bookmark without first selecting one") 
		} //end if
	}
	else if (windowName == "Precursors/MainWindow/Online/Login")
	{
		CS_REPORT (NOTIFY, "Waiting for the mouse to find the cheese...")

		windowManager->HideAll ();

		iPreGUIWindow* progress = windowManager->LoadWindow ("Progress");
		progress->Show ();

		//TODO: Take care of these in the Progress window class.
		progress->GetWindow ()->setText ("Loading...");
		CEGUI::Window* progMsg = ceguiWinMgr->getWindow ("Precursors/ProgressWindow/Text");
		progMsg->setText ("Waiting for the mouse to find the cheese...");

		// Render CEGUI's windows.
		csRef<iGraphics3D> g3d = csQueryRegistry<iGraphics3D> (object_reg);
		g3d->BeginDraw (CSDRAW_2DGRAPHICS);
		cegui->Render ();

		// Start the client
		//ServerInfo* server = serverData.GetElementPointer (selectedServer);
		/*csRef<iPreContentManager> contentManager = csQueryRegistry<iPreContentManager> (object_reg);
		if (server)
		{
			if (!(contentManager->StartClient (0, server->address, server->port)))
			{
				CS_REPORT (ERROR, "Could not start the client!")
				Show ();
			} // end if
		} // end if */
	}
	else if (windowName == "Precursors/MainWindow/Local/Start")
	{
		CS_REPORT (NOTIFY, "Hold on, the hamster is working...")

		windowManager->HideAll ();

		iPreGUIWindow* progress = windowManager->LoadWindow ("Progress");
		progress->Show ();

		//TODO: Take care of these in the Progress window class.
		progress->GetWindow ()->setText ("Loading...");
		CEGUI::Window* progMsg = ceguiWinMgr->getWindow ("Precursors/ProgressWindow/Text");
		progMsg->setText ("Hold on, the hamster is working...");

		// Render CEGUI's windows.
		csRef<iGraphics3D> g3d = csQueryRegistry<iGraphics3D> (object_reg);
		g3d->BeginDraw (CSDRAW_2DGRAPHICS);
		cegui->Render ();

		// Get the settings needed for the server.
		CEGUI::String portString = ceguiWinMgr->getWindow ("Precursors/MainWindow/Local/Port")->getProperty ("Text");
		unsigned int port;
		sscanf (portString.c_str (), "%u", &port);

		CEGUI::String levelString = ceguiWinMgr->getWindow ("Precursors/MainWindow/Local/Level")->getProperty ("Text");

		CEGUI::String serverName = ceguiWinMgr->getWindow ("Precursors/MainWindow/Local/Name")->getProperty ("Text");
		applicationConfig->SetInt ("Precursors.Server.ListenPort", port);
		applicationConfig->SetStr ("Precursors.Server.Name", serverName.c_str ());

		/*csRef<iPreContentManager> contentManager = csQueryRegistry<iPreContentManager> (object_reg);

		// Start the server
		if (!(contentManager.IsValid () && contentManager->StartServer (port, levelString.c_str ())))
		{
			CS_REPORT (ERROR, "Could not start the server!")
			Show ();
		} // end if*/
	}
	else
	{
		return false;
	} // end if
	return true;
} // end Action ()

void preWindowMain::RemoveServer (const char* name)
{
	if (serverData.In (name))
	{
		csString bookmarkName = "Precursors.Client.ServerBookmarks.";
		bookmarkName.Append (name);

		// Remove the data for this server.
		serverData.DeleteAll (name);
		applicationConfig->DeleteKey (bookmarkName);

		// Remove the server's item from the server list.
		ceguiWinMgr->destroyWindow ((const char*) (csString ("Precursors/MainWindow/Online/ServerList/") + name));

		// Disable the login button and show an appropriate message.
		ceguiWinMgr->getWindow ("Precursors/MainWindow/Online/Login")->disable ();
		selectedServer = "";
		status->setText ("Please select a server to connect to.");
		status->setProperty ("HorzFormatting", "WordWrapCentred");
	}
	else
	{
		csReport (object_reg, CS_REPORTER_SEVERITY_ERROR, "precursors.addon.window.main", "Couldn't find the server bookmark named '%s'!", name);
	} // end if
} // end RemoveServer ()

void preWindowMain::AddServer (const char* name, const char* address, uint port, bool addToConfigFile)
{
	if (!serverData.In (name))
	{
		// Add an entry to the server listbox.
		CEGUI::ItemEntry* tempEntry = (CEGUI::ItemEntry*) ceguiWinMgr->createWindow ("SleekSpace/ListboxItem", (const char*) (csString ("Precursors/MainWindow/Online/ServerList/") + name));
		tempEntry->setText (name);
		serverList->addItem (tempEntry);

		// Subscribe to all mouse click events in this window
		CEGUI::Event::Connection conn = tempEntry->subscribeEvent (CEGUI::ItemEntry::EventSelectionChanged,
			CEGUI::Event::Subscriber (&preWindowMain::OnServerSelected, this));
		serverListConnections.Push (conn);

		// Keep track of the server information.
		if (port == 0)
		{
			serverData.Put (name, ServerInfo (address));
		}
		else
		{
			serverData.Put (name, ServerInfo (address, port));
		} // end if

		// Add the server to the configuration file, if needed.
		if (addToConfigFile)
		{
			// Add bookmark to the configuration file.
			csString bookmarkName = "Precursors.Client.ServerBookmarks.";
			bookmarkName.Append (name);
			if (port == 0)
			{
				applicationConfig->SetStr (bookmarkName, address);
			}
			else
			{
				csString bookmark;
				bookmark.Format ("%s:%u", address, port);
				applicationConfig->SetStr (bookmarkName, bookmark);
			} // end if
		} // end if
	}
	else
	{
		csReport (object_reg, CS_REPORTER_SEVERITY_ERROR, "precursors.addon.window.main", "A server is already bookmarked with the name '%s'!", name);
	} // end if
} // end AddServer ()

void preWindowMain::ModifyServer (const char* oldName, const char* newName, const char* address, uint port, bool addToConfigFile)
{
	
	if (!serverData.In (newName) || (csString (oldName) == csString (newName)))
	{
	   if (serverData.In(oldName))
		{
			// Find an entry in the server's listbox.
			CEGUI::ItemEntry* tempEntry = (CEGUI::ItemEntry*) ceguiWinMgr->getWindow ((const char*) (csString ("Precursors/MainWindow/Online/ServerList/") + oldName));

			// Subscribe to all mouse click events in this window
			CEGUI::Event::Connection conn = tempEntry->subscribeEvent (CEGUI::ItemEntry::EventSelectionChanged,
				CEGUI::Event::Subscriber (&preWindowMain::OnServerSelected, this));
			serverListConnections.Push (conn);

			if (csString (oldName) != csString (newName))
			{
				ceguiWinMgr->renameWindow ((CEGUI::Window*)tempEntry, (const char*)(csString("Precursors/MainWindow/Online/ServerList/") + newName));
				tempEntry->setText (newName);
			} // end if

			// Keep track of the server information.
			if (port == 0)
			{
				serverData.DeleteAll (oldName);
				serverData.Put (newName, ServerInfo (address));
			}
			else
			{
				serverData.DeleteAll (oldName);
				serverData.Put (newName, ServerInfo (address, port));
			} // end if

			// Add the server to the configuration file, if needed.
			if (addToConfigFile)
			{
				// Add bookmark to the configuration file.
				csString bookmarkName = "Precursors.Client.ServerBookmarks.";
				csString oldbookmarkName = "Precursors.Client.ServerBookmarks.";
				
				bookmarkName.Append (newName);
				oldbookmarkName.Append (oldName);

				csReport (object_reg, CS_REPORTER_SEVERITY_NOTIFY, "precursors.addon.window.modifyserver", "Modifying server from %s to %s", oldbookmarkName.GetData (), bookmarkName.GetData ());
				if (port == 0)
				{
					applicationConfig->DeleteKey (oldbookmarkName);
					applicationConfig->SetStr (bookmarkName, address);
				}
				else
				{
					applicationConfig->DeleteKey (oldbookmarkName);
					csString bookmark;
					bookmark.Format ("%s:%u", address, port);
					applicationConfig->SetStr (bookmarkName, bookmark);
				} // end if
			} // end if
			
			if ((selectedServer == newName) || (selectedServer == oldName))
			{
				csString stat;
				stat.Format ("Address: %s\nPort: %u", address, port);
				status->setText (stat.GetData ());
				status->setProperty ("HorzFormatting", "LeftAligned");
				selectedServer = newName;
			} // end if
		}
		else
		{
			csReport (object_reg, CS_REPORTER_SEVERITY_ERROR, "precursors.addon.window.main", "A server by the name of '%s' does not exist!", oldName);
		} // end if
	}
	else
	{
		csReport (object_reg, CS_REPORTER_SEVERITY_ERROR, "precursors.addon.window.main", "A server is already bookmarked with the name '%s'!", newName);
	} // end if
} // end ModifyServer ()

bool preWindowMain::OnServerSelected (const CEGUI::EventArgs& args)
{
	CEGUI::ItemEntry* item =
		(CEGUI::ItemEntry*) (static_cast <const CEGUI::WindowEventArgs&> (args).window);

	if (item != NULL && item->isSelected ())
	{
		ceguiWinMgr->getWindow ("Precursors/MainWindow/Online/Login")->enable ();
		selectedServer = item->getName ().c_str ();
		size_t pos = selectedServer.FindLast ('/');
		selectedServer = selectedServer.Slice (pos + 1);
		ServerInfo* server = serverData.GetElementPointer (selectedServer);
		if (server)
		{
			csString stat;
			stat.Format ("Address: %s\nPort: %u", server->address.GetData (), server->port);
			status->setText (stat.GetData ());
			status->setProperty ("HorzFormatting", "LeftAligned");
		}
		else
		{
			csReport (object_reg, CS_REPORTER_SEVERITY_ERROR, "precursors.addon.window.main", "Couldn't find data for server '%s'!", selectedServer.GetData ());
		} // end if
	}
	else
	{
		ceguiWinMgr->getWindow ("Precursors/MainWindow/Online/Login")->disable ();
		selectedServer = "";
		status->setText ("Please select a server to connect to.");
		status->setProperty ("HorzFormatting", "WordWrapCentred");
	} // end if

	return false;
} // end OnServerSelected ()

/// @}


////////////////////////////////////////////////////////////////////////
/// Helper Methods
/// @{

void preWindowMain::PopulateForms ()
{
	// Populate the list of server bookmarks.
	csRef<iConfigIterator> it = applicationConfig->Enumerate ("Precursors.Client.ServerBookmarks.");
	csString temp;
	if (it.IsValid ())
	{
		while (it->Next ())
		{
			temp = it->GetKey ();
			temp = (temp.Slice (strlen (it->GetSubsection ())));
			AddServer (temp, it->GetStr (), 0, false);
		} // end while
	} // end if

	// Populate the Local Server settings
	if (applicationConfig->KeyExists ("Precursors.Server.ListenPort"))
	{
		int port = applicationConfig->GetInt("Precursors.Server.ListenPort");
		CEGUI::ItemEntry* tempPort = (CEGUI::ItemEntry*) ceguiWinMgr->getWindow ((const char*) (csString ("Precursors/MainWindow/Local/Port") ));
		csString portstring;
		portstring.Format ("%u", port);
		tempPort->setText ((const char *)portstring);
	} // end if

	if (applicationConfig->KeyExists ("Precursors.Server.Name"))
	{
		csString serverName = applicationConfig->GetStr ("Precursors.Server.Name");
		CEGUI::ItemEntry* tempName = (CEGUI::ItemEntry*) ceguiWinMgr->getWindow ((const char*) (csString ("Precursors/MainWindow/Local/Name") ));
		tempName->setText ((const char *)serverName);
	} // end if

	// Populate the list of available levels.
	/* csRef<iPreContentManager> contentManager = csQueryRegistry<iPreContentManager> (object_reg);
	if (contentManager)
	{
		CEGUI::Combobox* levelsCombo = (CEGUI::Combobox*) ceguiWinMgr->getWindow ("Precursors/MainWindow/Local/Level");
		if (levelsCombo != NULL)
		{
			csString level, level_trimmed;
			CEGUI::ListboxItem* lbi;
			csRef<iStringArray> levels = contentManager->GetLevelList ();
			for (size_t idx = 0; idx < levels->GetSize (); idx++)
			{
				printf ("Adding level '%s'.\n", levels->Get (idx));
				fflush (stdout);
				lbi = new CEGUI::ListboxTextItem (CEGUI::String (levels->Get (idx)), idx);
				levelsCombo->addItem (lbi);
			} // end for
		}
		else
		{
			CS_REPORT (WARNING, "Couldn't find the Levels combo box!")
		} // end if	
	}
	else
	{
		CS_REPORT (ERROR, "Couldn't find the content manager!")
	} // end if*/	
} // end PopulateForms ()

/// @}

