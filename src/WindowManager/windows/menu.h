////////////////////////////////////////////////////////////////////////////////
/**
 * Copyright (C) 2004-2006 G33X Nexus Entertainment (http://gne.g33xnexus.com/)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////
/**
 * @file preWindowMenu.h
 * @author David Bronke <David.Bronke@g33xnexus.com>
 *
 * @brief Declaration of the Precursors GUI window base class.
 */
////////////////////////////////////////////////////////////////////////////////


#ifndef __preWindowMenu_h__
#define __preWindowMenu_h__

#include "iwindowmanager/preGUIWindow.h"

// Forward declarations
struct iObjectRegistry;


/**
 * The content loader interface.
 */
struct preWindowMenu : public scfImplementationExt0<preWindowMenu, preGUIWindow>
{
public:
	////////////////////////////////////////////////////////////////////////
	/// Constructors and destructors
	/// @{

	/// Constructor.
	preWindowMenu (iObjectRegistry* object_reg);

	/// Destructor.
	virtual ~preWindowMenu ();

	/// @}


	////////////////////////////////////////////////////////////////////////
	/// Window Management Methods
	/// @{

	/**
	 * Send this window a message.
	 * @return true on success, false otherwise.
	 */
	virtual bool SendMessage (const char* message, csArray<csVariant> parameters);

	/// @}


	////////////////////////////////////////////////////////////////////////
	/// Event Handlers
	/// @{

	/// Perform the action associated with the given control.
	virtual bool Action (const CEGUI::String windowName);

	/// @}
}; // end struct preWindowMenu

#endif // __preWindowMenu_h__

