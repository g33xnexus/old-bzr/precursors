////////////////////////////////////////////////////////////////////////////////
/**
 * Copyright (C) 2004-2006 G33X Nexus Entertainment (http://gne.g33xnexus.com/)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////
/**
 * @file WindowManager.h
 *
 * @author David Bronke <David.Bronke@g33xnexus.com>
 *
 * @brief Declaration of WindowManager, the game content loader class.
 */
////////////////////////////////////////////////////////////////////////////////


#ifndef __WindowManager_h__
#define __WindowManager_h__

#include "global.h"

// CS Includes
#include "csutil/ref.h"
#include "csutil/weakref.h"
#include "csutil/refarr.h"
#include "csutil/csbaseeventh.h"
#include "csutil/csstring.h"
#include "csutil/stringarray.h"
#include "cstool/csapplicationframework.h"

#include "ivideo/graph3d.h"
#include "imap/loader.h"

// CEL Includes
#include "physicallayer/entity.h"
#include "physicallayer/pl.h"
#include "behaviourlayer/bl.h"

// Precursors Includes
#include "iwindowmanager/iPreWindowManager.h"


// Forward declarations
struct iObjectRegistry;
struct iEvent;
struct iVFS;
struct iGraphics3D;
struct iCEGUI;

struct iCelEntity;

//struct iPreContentManager;
struct iPreGUIWindow;

namespace CEGUI
{
	struct Window;
	struct WindowManager;
	struct MouseCursor;
} // end namespace CEGUI

/// All window events must be at least 300ms apart to be handled.
#define WINDOW_EVENT_DELTA 300


namespace Precursors
{
	////////////////////////////////////////////////////////////////////////////
	/**
	 * @brief The game content loader class.
	 *
	 * Loads a given level.xml file, loading the appropriate world and creating
	 * regions and zones as defined in the xml file, then loads, instantiates,
	 * and positions the entities referenced within the loaded worldfile,
	 */
	class WindowManager : public scfImplementation2<WindowManager, iPreWindowManager,
		iEventHandler>
	{
	public:
		////////////////////////////////////////////////////////////////////////////
		/// Constructor and Destructor
		/// @{

		/**
		 * Constructor.
		 */
		WindowManager (iObjectRegistry* object_reg, const char* configprefix);

		/**
		 * Destructor.
		 */
		~WindowManager ();

		/// @}


		////////////////////////////////////////////////////////////////////////////
		/// iPreWindowManager implementation
		/// @{

		/**
		 * Set the given window as the root window.
		 */
		virtual void SetRootWindow (const char* window);

		/**
		 * Toggle the root between two different windows.
		 * @return true if the root window was set to windowName1; false otherwise.
		 */
		virtual bool ToggleRootWindow (iPreGUIWindow* window1, iPreGUIWindow* window2);

		/**
		 * Get a pointer to the root window.
		 */
		virtual iPreGUIWindow* GetRootWindow ();

		/**
		 * Set a property on a child the given window.
		 */
		virtual void SetChildProperty (const char* winName, const char* property, const char* value);

		/**
		 * Find the window with the given name.
		 */
		virtual iPreGUIWindow* FindWindow (const char* windowName);

		/**
		 * Load the given window and return a pointer to it.
		 */
		virtual iPreGUIWindow* LoadWindow (const char* windowName);

		/**
		 * Load the given window layout and return a pointer to the window.
		 */
		virtual CEGUI::Window* LoadCEGUILayout (const char* layoutName);

		/**
		 * Load the given window and return a pointer to it.
		 */
		virtual CEGUI::Window* LoadCEGUIWindow (const char* windowName);

		/**
		 * Hide all windows except the root window.
		 */
		virtual void HideAll ();

		/**
		 * Hide the mouse cursor and pass mouse events to the game logic.
		 */
		virtual void EnableMouseLook();

		/**
		 * Show the mouse cursor and pass mouse events to the GUI.
		 */
		virtual void DisableMouseLook();

		/// @}


		////////////////////////////////////////////////////////////////////////////
		/// iEventHandler implementation
		/// @{

		/// Event handler
		virtual bool HandleEvent (iEvent &event);

		/// @}


	private:
		////////////////////////////////////////////////////////////////////////////
		/// Private variables
		/// @{

		/// The object registry.
		iObjectRegistry* object_reg;
		
		/// Event handler ID
		csHandlerID self;
		
		/// A reference to the event eventQueue.
		csWeakRef<iEventQueue> eventQueue;

		/// A reference to the content loader.
		//csWeakRef<iPreContentManager> contentManager;

		/// The prefix for the configuration keys
		csString cfgkeyprefix;

		/// A reference to the configuration manager.
		csWeakRef<iConfigManager> configManager;

		/// A reference to vfs.
		csWeakRef<iVFS> vfs;

		/// A reference to the 3D graphics plugin.
		csWeakRef<iGraphics3D> g3d;

		/// A reference to the CEGUI plugin.
		csWeakRef<iCEGUI> cegui;

		/// A reference to the plugin manager.
		csWeakRef<iPluginManager> pluginManager;

		/// A reference to the CEGUI window manager.
		CEGUI::WindowManager* ceguiWindowManager;

		/// A reference to the CEGUI window manager.
		CEGUI::MouseCursor* ceguiMouseCursor;

		/// A pointer to the current root window.
		csWeakRef<iPreGUIWindow> rootWindow;

		/// Pointers to the windows we've created.
		csHash<csRef<iPreGUIWindow>, csString> windows;

		/// The stack of currently open windows.
		csStringArray windowStack;

		/// The time of the last event handled.
		csTicks lastTime;

		/// @}


		////////////////////////////////////////////////////////////////////////////
		/// Helper methods
		/// @{

		/**
		 * Load any default values for the given window.
		 */
		void LoadWindowDefaults (CEGUI::Window* win);

		/**
		 * Fill the given window with any data it needs.
		 */
		void FillWindow (CEGUI::Window* win, csString wName);

		/**
		 * Load an XML document from a file. Returns 0 on failure.
		 * Error will be reported already.
		 */
		csPtr<iDocument> LoadXML (const char* file);

		/**
		 * Load the entity loader for a given entity type.
		 * Returns 0 on failure. Error already reported.
		 */
		iDocumentNode* LoadEntityLoader (const char* entity_type);

		/// Create the level entity.
		iCelEntity* CreateLevelEntity ();

		/// Declare the name of this event handler.
		CS_EVENTHANDLER_NAMES("application.Precursors.WindowManager")
	
		/*
		 * Declare that we're not terribly interested in having events
		 * delivered to us before or after other modules, plugins, etc.
		 */
		CS_EVENTHANDLER_NIL_CONSTRAINTS

		/// @}
	}; // end class WindowManager
} // end namespace Precursors

#endif // __WindowManager_h__
