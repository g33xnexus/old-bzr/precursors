////////////////////////////////////////////////////////////////////////////////
/**
 * Copyright (C) 2004-2006 G33X Nexus Entertainment (http://gne.g33xnexus.com/)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////
/**
 * @file WindowManager.cpp
 *
 * @author David Bronke <David.Bronke@g33xnexus.com>
 *
 * @brief Definition of Precursors, the main application object.
 */
////////////////////////////////////////////////////////////////////////////////


#include "WindowManager.h"

// CS Includes
#include "iutil/vfs.h"
#include "iutil/document.h"
#include "iutil/cfgmgr.h"
#include "iutil/cmdline.h"
#include "iutil/databuff.h"
#include "iutil/eventq.h"
#include "iutil/plugin.h"
#include "iutil/stringarray.h"
#include "csutil/event.h"
#include "csutil/csevent.h"
#include "csutil/objreg.h"
#include "iengine/mesh.h"
#include "iengine/movable.h"
#include "iengine/sector.h"
#include "iengine/region.h"
#include "csutil/cfgacc.h"
#include "cstool/keyval.h"
#include "ivaria/reporter.h"
#include "ivaria/dynamics.h"
#include "ivaria/ode.h"
#include "ivaria/icegui.h"
#include "ivideo/graph2d.h"
#include "ivideo/graph3d.h"
#include "ivideo/fontserv.h"

// CEL Includes
#include "physicallayer/propclas.h"
#include "propclass/input.h"
#include "propclass/mechsys.h"
#include "propclass/mesh.h"
#include "propclass/camera.h"
#include "propclass/simpcam.h"
#include "propclass/zone.h"
#include "tools/entityloader.h"

// CEGUI Includes
#include "CEGUI.h"
#include "elements/CEGUICombobox.h"
#include "elements/CEGUIListboxTextItem.h"

// Precursors Includes
//#include "iPreContentManager.h"
//#include "iClientNetworkLayer.h"
//#include "iServerNetworkLayer.h"
#include "iwindowmanager/PrecursorsEvents.h"
#include "iwindowmanager/GUIEventInjector.h"
#include "iwindowmanager/iPreGUIWindow.h"

#include "windows/main.h"
#include "windows/menu.h"
#include "windows/addserver.h"
#include "windows/modifyserver.h"
#include "windows/query.h"
#include "windows/targetreticle.h"

#define CS_REPORT(severity,message) csReport (object_reg, CS_REPORTER_SEVERITY_##severity, "precursors.addon.windowmanager", message);


namespace Precursors
{
	////////////////////////////////////////////////////////////////////////////
	// Constructor and Destructor.

	WindowManager::WindowManager (iObjectRegistry* object_reg, const char* configprefix)
		: scfImplementationType (this), self (CS_EVENT_INVALID)
	{
		WindowManager::object_reg = object_reg;
		cfgkeyprefix = configprefix;

		rootWindow = 0;

		// Setup event handler
		self = csEventHandlerRegistry::GetID (object_reg, this);

		/*contentManager = csQueryRegistry<iPreContentManager> (object_reg);
		if (!contentManager.IsValid ())
		{
			CS_REPORT (ERROR, "Can't find the content loader!")
		} // end if*/

		configManager = csQueryRegistry<iConfigManager> (object_reg);
		if (!configManager.IsValid ())
		{
			CS_REPORT (ERROR, "Can't find the configuration manager!")
		} // end if

		vfs = csQueryRegistry<iVFS> (object_reg);
		if (!vfs.IsValid ())
		{
			CS_REPORT (ERROR, "Can't find the virtual file system!")
		} // end if

		g3d = csQueryRegistry<iGraphics3D> (object_reg);
		if (!g3d.IsValid ())
		{
			CS_REPORT (ERROR, "Can't find the 3D graphics plugin!")
		} // end if

		cegui = csQueryRegistry<iCEGUI> (object_reg);
		if (!cegui.IsValid ())
		{
			CS_REPORT (ERROR, "Can't find the CEGUI plugin!")
		} // end if

		ceguiWindowManager = cegui->GetWindowManagerPtr ();
		if (ceguiWindowManager == NULL)
		{
			CS_REPORT (ERROR, "Can't find the CEGUI window manager!")
		} // end if

		ceguiMouseCursor = cegui->GetMouseCursorPtr ();
		if (ceguiMouseCursor == NULL)
		{
			CS_REPORT (ERROR, "Can't find the CEGUI mouse cursor class!")
		} // end if

		eventQueue = csQueryRegistry<iEventQueue> (object_reg);
		if (!eventQueue.IsValid ())
		{
			CS_REPORT (ERROR, "Can't find the event eventQueue!")
		} // end if

		pluginManager = csQueryRegistry<iPluginManager> (object_reg);
		if (!pluginManager.IsValid ())
		{
			CS_REPORT (ERROR, "Can't find the plugin loader!")
		} // end if

		eventQueue->RegisterListener (this);
		eventQueue->Subscribe (this, csevPostProcess (object_reg));
	} // end WindowManager ()

	WindowManager::~WindowManager ()
	{
		if (eventQueue)
		{
			
			eventQueue->RemoveListener (this);
		}

		if (object_reg)
		{
			csEventHandlerRegistry::ReleaseID (object_reg, this);
		}
	} // end ~WindowManager ()

	////////////////////////////////////////////////////////////////////////////
	// Window management.

	iPreGUIWindow* WindowManager::FindWindow (const char* windowName)
	{
		csString wName = windowName;

		iPreGUIWindow* win = NULL;

		// Load layout if needed and set as root
		if (windows.In (wName))
		{
			win = windows.Get (wName, NULL);
		}

		return win;
	} // end FindWindow ()

	void WindowManager::HideAll ()
	{
		CEGUI::Window* win = rootWindow->GetWindow ();
		CEGUI::String childName;
		CEGUI::Window* child = NULL;
		size_t numChildren = win->getChildCount ();

		for (uint idx = 0; idx < numChildren; idx++)
		{
			child = win->getChildAtIdx (idx);
			childName = child->getName ();
			child->hide ();
		} // end for
	} // end HideAll ()

	void WindowManager::SetRootWindow (const char* window)
	{
		if (rootWindow != NULL)
		{
			// Hide the root window and all children, and remove the children.
			rootWindow->Hide ();
			rootWindow = NULL;
		} // end if

		rootWindow = LoadWindow (window);
		rootWindow->Show ();
		cegui->GetSystemPtr ()->setGUISheet (rootWindow->GetWindow ());
	} // end SetRootWindow ()

	bool WindowManager::ToggleRootWindow (iPreGUIWindow* window1, iPreGUIWindow* window2)
	{
		//FIXME: This no longer works since we've moved the windows to their own classes.
		if (rootWindow == window1)
		{
			//SetRootWindow (window2);
			return false;
		}
		else
		{
			//SetRootWindow (window1);
			return true;
		} // end if
	} // end ToggleRootWindow ()

	iPreGUIWindow* WindowManager::GetRootWindow ()
	{
		return rootWindow;
	} // end GetRootWindow ()

	/**
	 * Set a property on the given window.
	 */
	void WindowManager::SetChildProperty (const char* winName, const char* property, const char* value)
	{
		CEGUI::WindowManager* ceguiWindowManager = cegui->GetWindowManagerPtr ();

		if (ceguiWindowManager->isWindowPresent (winName))
		{
			ceguiWindowManager->getWindow (winName)->setProperty (property, value);
		} // end if
	} // end SetChildProperty ()

	/**
	 * Load the given window and return a pointer to it.
	 */
	iPreGUIWindow* WindowManager::LoadWindow (const char* windowName)
	{
		printf ("Loading window '%s'.\n", windowName);
		fflush (stdout);

		csString wName = windowName;
		csRef<iPreGUIWindow> win;
		if (wName == "TargetingReticle")
		{
			win = new preWindowTargetingReticle (object_reg);
		}
		else
		{
			win = windows.Get (wName, NULL);
			if (!win.IsValid ())
			{
				if (wName == "Root")
				{
					win = new preGUIWindow (object_reg, "menu/root.layout");
				}
				else if (wName == "Main")
				{
					win = new preWindowMain (object_reg);
				}
				else if (wName == "Menu")
				{
					win = new preWindowMenu (object_reg);
				}
				else if (wName == "AddServer")
				{
					win = new preWindowAddServer (object_reg);
				}
				else if (wName == "ModifyServer")
				{
					win = new preWindowModifyServer (object_reg);
				}
				else if (wName == "Query")
				{
					win = new preWindowQuery (object_reg);
				}
				else if (wName == "Progress")
				{
					//TODO: Create a class for the progress dialog!
					win = new preGUIWindow (object_reg, "dialogs/progress.layout");
				}
				else if (wName == "Hud")
				{
					//TODO: Create a class for the HUD!
					win = new preGUIWindow (object_reg, "hud/hud.layout");
				} // end if

				if (win.IsValid ())
				{
					windows.Put (wName, win);
				} // end if
			} // end if
		} // end if
		return win;
	} // end LoadWindow ()

	/**
	 * Load the given window layout and return a pointer to the window.
	 */
	CEGUI::Window* WindowManager::LoadCEGUILayout (const char* layoutName)
	{
		csString wName = layoutName;
		size_t split = wName.FindLast ('/');
		vfs->PushDir ("/precursors/gui/layouts/" + wName.Slice (0, split));
		CEGUI::Window* win = ceguiWindowManager->loadWindowLayout (wName.Slice (split + 1).GetData ());

		if (rootWindow.IsValid () && win != NULL)
		{
			rootWindow->GetWindow ()->addChildWindow (win);
		} // end if
		return win;
	} // end LoadCEGUILayout ()

	/**
	 * Load the given window and return a pointer to it.
	 */
	CEGUI::Window* WindowManager::LoadCEGUIWindow (const char* windowName)
	{
		CEGUI::Window* win = ceguiWindowManager->createWindow (windowName);

		if (rootWindow.IsValid () && win != NULL)
		{
			rootWindow->GetWindow ()->addChildWindow (win);
		} // end if
		return win;
	} // end LoadCEGUIWindow ()

	bool WindowManager::HandleEvent (iEvent &event)
	{
		if (event.Name == csevPostProcess (object_reg))
		{
			// Render CEGUI's windows.
			g3d->BeginDraw (CSDRAW_2DGRAPHICS);
			cegui->Render ();

			return true;
		} // end if

		return false;
	} // end HandleEvent ()

	void WindowManager::EnableMouseLook ()
	{
		ceguiMouseCursor->hide ();
		cegui->DisableMouseCapture ();
	} // end EnableMouseLook ()

	void WindowManager::DisableMouseLook ()
	{
		ceguiMouseCursor->show ();
		cegui->EnableMouseCapture ();
	} // end DisableMouseLook ()
} // end namespace Precursors

