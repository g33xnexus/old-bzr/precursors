////////////////////////////////////////////////////////////////////////////////
/**
 * Copyright (C) 2004-2006 G33X Nexus Entertainment (http://gne.g33xnexus.com/)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////
/**
 * @file	preClient.cpp
 * @author	Christophers S. Case
 * @email	chris.case@g33xnexus.com
 *
 * @brief  Definition of preClient, the main application class.
 * 
 */
/////////////////////////////////////////////////////////////////////////////////*

#include "preClient.h"

// CS Includes
#include "iutil/vfs.h"
#include "csutil/sysfunc.h"
#include "csutil/cmdhelp.h"
#include "csutil/event.h"
#include "csutil/cfgacc.h"
#include "csutil/csstring.h"
#include "csutil/syspath.h"
#include "csutil/strhash.h"
#include "csutil/inputdef.h"
#include "cstool/collider.h"
#include "iutil/event.h"
#include "iutil/eventq.h"
#include "iutil/document.h"
#include "iutil/cmdline.h"
#include "iutil/csinput.h"
#include "iutil/databuff.h"
#include "iutil/plugin.h"
#include "iengine/engine.h"
#include "iengine/sector.h"
#include "iengine/lod.h"
#include "iengine/region.h"
#include "iengine/camera.h"
#include "iengine/mesh.h"
#include "iengine/movable.h"
#include "ivideo/fontserv.h"
#include "ivideo/graph2d.h"
#include "ivideo/graph3d.h"
#include "ivideo/natwin.h"
#include "igraphic/imageio.h"
#include "ivaria/reporter.h"
#include "ivaria/stdrep.h"
#include "ivaria/collider.h"
#include "ivaria/dynamics.h"
#include "ivaria/ode.h"
#include "ivaria/icegui.h"
#include "ivaria/conin.h"
#include "imap/loader.h"

// CEL Includes
#include "celtool/initapp.h"
#include "physicallayer/persist.h"
#include "physicallayer/propclas.h"
#include "propclass/camera.h"
#include "propclass/simpcam.h"
#include "propclass/linmove.h"
#include "propclass/actormove.h"
#include "propclass/mechsys.h"
#include "propclass/mechthruster.h"
#include "propclass/zone.h"
#include "tools/entityloader.h"
#include "behaviourlayer/bl.h"

// CEGUI Includes
#include <CEGUI.h>

// Precursors Includes
#include "WindowManager/WindowManager.h"
#include "iwindowmanager/iPreGUIWindow.h"


namespace Precursors
{
	////////////////////////////////////////////////////////////////////////////
	// Constructor and Destructor.

	preClient::preClient () : csApplicationFramework ()
	{
		SetApplicationName ("gne.Precursors");
		cfgkeyprefix = csString ("Precursors.");
	} // end preClient ()

	preClient::~preClient ()
	{
	} // end ~preClient ()

	////////////////////////////////////////////////////////////////////////////
	// Initialization functions.

	void preClient::PostProcessFrame()
	{
	  if (g3d->BeginDraw(engine->GetBeginDrawFlags() | CSDRAW_3DGRAPHICS))
	  {
		// Draw frame.
	  }
	}

	void preClient::FinishFrame()
	{
	  g3d->FinishDraw();
	  g3d->Print(0);
	}

	bool preClient::OnKeyboard(iEvent& ev)
	{
	  // We got a keyboard event.
	  if (csKeyEventHelper::GetEventType(&ev) == csKeyEventTypeDown)
	  {
		// The user pressed a key (as opposed to releasing it).
		utf32_char code = csKeyEventHelper::GetCookedCode(&ev);
		if (code == CSKEY_ESC)
		{
		  // The user pressed escape, so terminate the application.  The proper way
		  // to terminate a Crystal Space application is by broadcasting a
		  // csevQuit event.  That will cause the main run loop to stop.  To do
		  // so we retrieve the event queue from the object registry and then post
		  // the event.
		  csRef<iEventQueue> q =
			csQueryRegistry<iEventQueue> (GetObjectRegistry());
		  if (q.IsValid())
			q->GetEventOutlet()->Broadcast(csevQuit(GetObjectRegistry()));
		}
	  }
	  return false;
	}

	bool preClient::OnInitialize(int argc, char* argv[])
	{
		iObjectRegistry* object_reg = GetObjectRegistry();
	  
		if (!OpenApplication(GetObjectRegistry()))
			return ReportError("Error opening system!");
 	
		// Get our references to the VFS plugin and the configuration manager.	
		vfs = csQueryRegistry<iVFS> (object_reg);
		if (!vfs)
		{
			return ReportError ("Can't find the virtual file system!");
		} // end if

		applicationConfig = csQueryRegistry<iConfigManager> (object_reg);
		if (!applicationConfig)
		{
			return ReportError ("Can't find the config manager!");
		} // end if

		// Load the common Precursors application configuration.
		applicationConfig->AddDomain ("/precursors/config/preCommon.cfg", vfs, 0);

		// Load the Precursors client configuration.
		applicationConfig->AddDomain ("/precursors/config/preClient.cfg", vfs, 1);

		// Create the user's configuration directory for Precursors if it doesn't already exist.

		// Load the user's configuration file.
		csRef<iConfigFile> userConfig = applicationConfig->AddDomain (
			"/user/config/preClient.cfg", vfs, 10);

		// Set the user's configuration to the dynamic domain.
		applicationConfig->SetDynamicDomain (userConfig);

		// RequestPlugins() will load all plugins we specify.  In addition it will
		// also check if there are plugins that need to be loaded from the
		// configuration system (both the application configuration and CS or global
		// configurations).  It also supports specifying plugins on the command line
		// via the --plugin= option.
		if (!csInitializer::RequestPlugins(object_reg,
			CS_REQUEST_VFS,
			CS_REQUEST_OPENGL3D,
			CS_REQUEST_ENGINE,
			CS_REQUEST_FONTSERVER,
			CS_REQUEST_IMAGELOADER,
			CS_REQUEST_LEVELLOADER,
			CS_REQUEST_REPORTER,
			CS_REQUEST_REPORTERLISTENER,
			CS_REQUEST_PLUGIN ("crystalspace.cegui.wrapper", iCEGUI),
			CS_REQUEST_END))
		return ReportError("Failed to initialize plugins!");

		// "Warm up" the event handler so it can interact with the world
		csBaseEventHandler::Initialize(GetObjectRegistry());
	 
		// Set up an event handler for the application.  Crystal Space is fully
		// event-driven.  Everything (except for this initialization) happens in
		// response to an event.
		if (!RegisterQueue (object_reg, csevAllEvents(GetObjectRegistry())))
			return ReportError("Failed to set up event handler!");
		
		return true;
		}

	void preClient::OnExit()
	{
	}

	bool preClient::Application()
	{
	  iObjectRegistry* r = GetObjectRegistry();

	  // Open the main system. This will open all the previously loaded plugins
	  // (i.e. all windows will be opened).
	  if (!OpenApplication(r))
		return ReportError("Error opening system!");

	  	g2d = csQueryRegistry<iGraphics2D> (object_reg);
		if (!g2d)
		{
			return ReportError("Failed to locate 2D renderer!");
		} // end if
		
		g2d->GetNativeWindow ()->SetTitle ("Requiem For Innocence: Precursors");

	  // Now get the pointer to various modules we need.  We fetch them from the
	  // object registry.  The RequestPlugins() call we did earlier registered all
	  // loaded plugins with the object registry.  It is also possible to load
	  // plugins manually on-demand.
	  g3d = csQueryRegistry<iGraphics3D> (r);
	  if (!g3d)
		return ReportError("Failed to locate 3D renderer!");

		/***************************************************
		 * Get pointers to the rest of the plugins we need *
		 ***************************************************/
		engine = csQueryRegistry<iEngine> (object_reg);
		if (!engine)
		{
			return ReportError("Failed to locate 3D engine!");
		} // end if

		cdsys = csQueryRegistry<iCollideSystem> (object_reg);
		if (!cdsys)
		{
			return ReportError ("Can't find the collision detection system!");
		} // end if

		mouse_driver = csQueryRegistry<iMouseDriver> (object_reg);
		if (!mouse_driver)
		{
			return ReportError ("Can't find the mouse driver!");
		} // end if

		loader = csQueryRegistry<iLoader> (object_reg);
		if (!loader)
		{
			return ReportError ("Can't find the map loader!");
		} // end if

		/*pl = csQueryRegistry<iCelPlLayer> (object_reg);
		if (!pl)
		{
			return ReportError ("Can't find the CEL physical layer!");
		} // end if*/

		/*dynamics = csQueryRegistry<iDynamics> (object_reg);
		if (!dynamics)
		{
			return ReportError ("Can't find the dynamics plugin!");
		} // end if*/

		cegui = csQueryRegistry<iCEGUI> (object_reg);
		if (!cegui)
		{
			return ReportError ("Can't find the CEGUI plugin!");
		} // end if

		csRef<iCommandLineParser> cmdline = csQueryRegistry<iCommandLineParser> (object_reg);
		if (!cmdline)
		{
			return ReportError ("Can't find the command line parser!");
		} // end if

		/****************
		 * Set up CEGUI *
		 ****************/
		// Initialize the CEGUI wrapper
		cegui->Initialize ();

		// Set the logging level
		cegui->GetLoggerPtr ()->setLoggingLevel (CEGUI::Informative);

		vfs->ChDir ("/precursors/gui/schemes/sleekspace/");

		// Load the sleekspace skin (which uses Falagard skinning system)
		cegui->GetSchemeManagerPtr ()->loadScheme ("sleekspace.scheme");

		cegui->GetSystemPtr ()->setDefaultMouseCursor ("SleekSpace", "MouseArrow");
		CEGUI::Font* font = cegui->GetFontManagerPtr ()->createFont ("FreeType", "Gemfont", "/fonts/Gemfont1.ttf");
		font->setProperty("PointSize", "12");
		font->load ();

		/***************************
		 * Load the window manager *
		 ***************************/

		// Create an instance of the content loader for loading levels and entities.
		windowManager = new WindowManager (object_reg, cfgkeyprefix);
		if (!object_reg->Register (windowManager, scfInterfaceTraits<iPreWindowManager>::GetName()))
		{
			return ReportError ("Can't register window manager with the object registry!");
		} // end if

		// Inform debug that everything initialized succesfully.
		ReportInfo ("Application initialized successfully.");

		windowManager->SetRootWindow ("Root");
		windowManager->LoadWindow ("Main")->Show ();

		// Start the default run/event loop.  This will return only when some code,
		// such as OnKeyboard(), has asked the run loop to terminate.
		Run();

	  return true;
	}
} //end Precursors namespace
