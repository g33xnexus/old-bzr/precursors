////////////////////////////////////////////////////////////////////////////////
/**
 * @file iNetworkServer.h
 * @author Christopher Case (macguyvok@gmail.com)
 * @author David Bronke (whitelynx@gmail.com)
 *
 * @brief Declaration of the network server interface.
 */
////////////////////////////////////////////////////////////////////////////////


#ifndef NETWORK_ENET_SERVER_H
#define NETWORK_ENET_SERVER_H

#include "inetwork/iNetworkServer.h"


/**
 * @brief The network server interface.
 *
 * This is a network server, which is able to listen for and handle multiple
 * incoming connections.
 */
class csENetNetworkServer :
	public scfImplementation2<csENetNetworkServer, iComponent, iNetworkServer>
{
public:
	csENetNetworkServer (iBase *piBase);
	virtual ~csENetNetworkServer ();

	/*
	 * iComponent interface
	 */
public:
	virtual bool Initialize (iObjectRegistry *obj_reg);

	/*
	 * iNetworkConnection interface
	 */
public:
	/**
	 * Set the packet factory. This object will be used to create all packet
	 * objects.
	 */
	void SetPacketFactory (iNetworkPacketFactory* factory);

	/**
	 * Listen for incoming connections on the given address.
	 */
	void Listen (iNetworkAddress* address);

	/**
	 * Accept and return the next incoming connection.
	 */
	iNetworkConnection* GetIncomingConnection (bool blocking = false);

	/**
	 * Send a packet to all connected clients.
	 */
	void Broadcast (iNetworkPacket* packet);

	/**
	 * Close all open connections.
	 */
	void DisconnectAll ();
}; // end struct csENetNetworkServer

#endif // NETWORK_ENET_SERVER_H

