////////////////////////////////////////////////////////////////////////////////
/**
 * @file connection.h
 * @author Christopher Case (macguyvok@gmail.com)
 * @author David Bronke (whitelynx@gmail.com)
 *
 * @brief Declaration of the network connection interface, which represents
 * a network connection between the local machine and a remote machine.
 */
////////////////////////////////////////////////////////////////////////////////


#ifndef NETWORK_ENET_CONNECTION_H
#define NETWORK_ENET_CONNECTION_H

#include "iutil/comp.h"

#include "inetwork/iNetworkConnection.h"


/**
 * @brief The network connection interface.
 *
 * This interface represents a network connection between two hosts.
 */
class csENetNetworkConnection :
	public scfImplementation2<csENetNetworkConnection, iComponent, iNetworkConnection>
{
public:
	csENetNetworkConnection (iBase *piBase);
	virtual ~csENetNetworkConnection ();

	/*
	 * iComponent interface
	 */
public:
	virtual bool Initialize (iObjectRegistry *obj_reg);

	/*
	 * iNetworkConnection interface
	 */
public:
	/**
	 * Set the packet factory. This object will be used to create all packet objects.
	 */
	void SetPacketFactory (iNetworkPacketFactory* factory);
	
	/**
	 * Send a packet.
	 */
	void Send (iNetworkPacket* packet);
	
	/**
	 * Receive a packet.
	 */
	iNetworkPacket* Receive (bool blocking = false);
	
	/**
	 * Disconnect and destroy connection. Returns True if successful.
	 */
	bool Disconnect ();
	
	/**
	 * Limit the amount of bandwith used by this connection.
	 */
	void Limit (unsigned int limit);
}; // end class csENetNetworkConnection

#endif // NETWORK_ENET_CONNECTION_H

